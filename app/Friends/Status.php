<?php
namespace App\Friends;

use Illuminate\Database\Eloquent\Model;

class Status extends Model{
    const STATUS_REQUEST    = 1;
    const STATUS_ACCEPTED   = 2;
    const STATUS_REMOVED    = 3;
    const STATUS_BLOCKED    = 4;
}