<?php

namespace App\Trade;

use Illuminate\Database\Eloquent\Model;

class Log extends Model{
    public $table = "trade_log";
    protected $guarded = [];

    const LOG_TYPE_CHAT = 3;
}
