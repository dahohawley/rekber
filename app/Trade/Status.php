<?php

namespace App\Trade;

use Illuminate\Database\Eloquent\Model;

class Status extends Model{
    const STATUS_REQUEST = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECTED = 2;
    const STATUS_CANCELED = 3;
    const STATUS_COMPLETE = 4;
}
