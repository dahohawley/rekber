<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Trade\Status;

class Trade extends Model{
    protected $table                = 'trade';
    protected $guarded              = [];
    public $timestamps              = true;
    const CREATED_AT = 'create_dtm';
    const UPDATED_AT = 'update_dtm';
    
    /**
     * biasa aja constant ini mah dipake untuk load data berdasarkan ID 
    */
    const LOADBY_ID         = 0;

    /**  
     * aktif request 
     * cari yang subject / ref_id nya sesuai $id
     * intinya kalo pake mode ini, cari yang partisipan di trade dan statusnya
     * masih request
    */
    const LOADBY_REQUESTED  = 1;

    const LIST_ALL          = 0;

    /**
     * digunakan untuk loadList data trade, yang requester_id nya = \Auth::user()->id;
     * dan statusnya = 0 (Masih Request)
     */
    const LISTBY_REQUESTING = 1;

    /**
     * digunakan untuk loadList data trade yang di request ke user, dan statusnya = 0 (Masih request)
     * $id diisi dengan subject_id yang mau dicari.
     */
    const LISTBY_REQUESTED = 2;

    /**
     * digunakan untuk loadList data trade yang aktif. $id digunakan untuk mencari
     * ref_id / subject_id nya
     */
    const LISTBY_ACTIVE = 3;

    /**
     * digunakan untuk memuat semua data transaksi yang dimiliki.
     */
    const LISTBY_OWNED      = 4;



    public function loadRow($id = null, $mode = self::LOADBY_ID, $params = array()){
        $builder        = \DB::table($this->table);

        switch($mode){
            case self::LOADBY_ID:
                $builder->where('trade_id',$id);
            break;
            case self::LOADBY_REQUESTED:
                if(!isset($params['subject_id'])){
                    throw new \Exception("\$params['subject_id'] required!",1);
                    return false;
                }
                $builder->where(function($nest) use(&$id){
                    $nest->where('requester_id','=',$id);
                    $nest->orWhere('subject_id','=',$id);
                });

                $builder->where(function($nest) use(&$params){
                    $subject_id     = $params['subject_id'];
                    $nest->where('requester_id','=',$subject_id);
                    $nest->orWhere('subject_id','=',$subject_id);
                });

                $builder->where('trade_status','=',Status::STATUS_REQUEST);

            break;
        }
        
        $load           = (array)$builder->first();
        if($load){
            $this->fill($load);
            return $load;
        }else{
            return false;
        }
    }

    /**
     * digunakan untuk load data secara massal
     * @param id digunakan jika mode != LIST_ALL sebagai identifier yang dicari sesuai deengan mode nya
     * @param mode digunakan untuk menentukan data akan dicari berdasarkan apa
     * @param params bisa diisi dengan limit, offset, dan parameter tambahan apabila dibutuhkan
     * @param deep set menjadi true untuk ngeload semua data foreign key nya, tapi lebih lama dikit keknya heuheuheuehu
     * 
     * @return array berisi list data
     */
    public function loadList($id = null, $mode = self::LIST_ALL, $params = array(), $deep = false){
        $builder        = \DB::table($this->table);

        if($mode !== self::LIST_ALL){
            switch ($mode) {
                case self::LISTBY_ACTIVE:
                    $builder->where(function($query) use($id){
                        $query->where('requester_id','=',$id);
                        $query->orWhere("subject_id",'=',$id);
                    });
                    $builder->where("trade_status",'=',Status::STATUS_ACCEPTED);
                break;
                case self::LISTBY_REQUESTING:
                    $builder->where('requester_id','=',$id);
                    $builder->where('trade_status','=',Status::STATUS_REQUEST);
                    break;
                case self::LISTBY_REQUESTED:
                    $builder->where('subject_id','=',$id);
                    $builder->where('trade_status','=',Status::STATUS_REQUEST);
                break;
                case self::LISTBY_OWNED:
                    $builder->where(function($query) use($id){
                        $query->where('requester_id','=',$id);
                        $query->orWhere("subject_id",'=',$id);
                    });
                break;
            }
        }

        /* limit & offset hanya di set ketika $params memiliki key limit / offset*/
        if(isset($params['limit'])){
            $builder->limit(intval($params['limit']));
            $builder->offset(intval($params['offset']));
        }
        if(isset($params['order'])){
            $column         = $params['order']['column'];
            $dir            = $params['order']['dir'];
            $builder->orderBy($column,$dir);
        }

        try {
            $list       = $builder->get();
            $rows       = array();
            if($list){
                foreach($list as $row){
                    if($deep){
                        // Load subject
                        $user       = new \App\User();
                        $user->loadRow($row->subject_id);
                        $dataSubject        = $user->removeConfidential($user->getAttributes());
                        $row->dataSubject   = $dataSubject;

                        // Load subject
                        $user                   = new \App\User();
                        $user->loadRow($row->requester_id);
                        $dataRequester          = $user->removeConfidential($user->getAttributes());
                        $row->dataRequester       = $dataRequester;

                        $userId                 = \Auth::id();
                        if($dataSubject['user_id'] == $userId){
                            $row->tradeWith         = $dataRequester;
                        }else{
                            $row->tradeWith         = $dataSubject;
                        }

                    }
                    $rows[]     = (array)$row;
                }
            }
            return $rows;
        } catch (\Throwable $th) {
            return false;
        }
    }


    /**
     * digunakan untuk mengetahui data yang telah dimuat, salah satunya objectnya merupakan 
     * user_id dari user yang login atau bukan
     * @param userId user yang mau dicheck apakah dia partisipant dalam sebuah trade
     * @param data data yang akan dicek. bisa dikosongkan, tapi $tradeId harus diisi
     * @param tradeId jika tradeId kosong, maka trade akan di muat dari database di fungsi ini
     */
    public function isParticipant($userId = null, $data = array(),$tradeId = null){
        $isParticipant  = false;

        if($tradeId){
            $builder        = \DB::table($this->table);
            $builder->where('trade_id','=',$tradeId);
            $data           = (array)$builder->first();
        }

        if(!$userId){
            $userId         = \Auth::id();
        }

        if(!$data){
            $data   = $this->getAttributes();
        }

        if($data && $userId){    
            if($data['requester_id'] == $userId || $data['subject_id'] == $userId){
                $isParticipant = true;
                $info          = "This person is a participant of this transaction";
            }
        }else if(!$data){
            $info               = "This transaction is not exists";
        }

        return array(
            'status'    => $isParticipant,
            'info'      => $info
        );
    }


}
