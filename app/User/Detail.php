<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;
use \App\User;

class Detail extends Model{
    protected $table            = 'acc_users_detail';
    public $timestamps           = false;
    protected $guarded          = [];

    const LOADBY_ID             = 0;
    const LOADBY_USER_ID        = 1;

    public function loadRow($id = null, $mode = self::LOADBY_ID){
        $builder        = \DB::table(\DB::raw($this->table.' as aud'));
        
        switch($mode){
            case self::LOADBY_ID:
                $builder->where('id','=',$id);
            break;
            case self::LOADBY_USER_ID:
                $builder->where('user_id','=',$id);
            break;
        }

        if($data = $builder->first()){
            $this->fill((array)$data);
            return $data;
        }else{
            return false;
        }       
    }
}
