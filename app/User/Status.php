<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'acc_user_status';
    
    const STATUS_REGISTER   = 1;
    const STATUS_ACTIVE     = 2;
    const STATUS_INREVIEW   = 3;
    const STATUS_NOTACTIVE  = 4;
    const STATUS_BANNED     = 5;
    const STATUS_LOCKED     = 6;

}
