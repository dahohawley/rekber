<?php

namespace App\Notification;

use Illuminate\Database\Eloquent\Model;

class Token extends Model{
    protected $table    = 'acc_user_token';
    protected $guarded    = [];
}
