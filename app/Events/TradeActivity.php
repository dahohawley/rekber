<?php

namespace App\Events;

use App\Chat;
use App\Trade;
use App\Trade\Log;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TradeActivity implements ShouldBroadcastNow{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $log;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Log $log){
        $this->log  = $log;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn(){
        return new PrivateChannel('trade.'.$this->log->trade_Id);
    }
    public function broadcastWith(){
        $log        =   $this->log;
        $payload    =   json_decode($log->data);
        
        // $trade      =   \App\Trade::where(array('trade_id' => $log->trade_id))->first()->getAttributes();

        if($log->type_id == \App\Trade\Log::LOG_TYPE_CHAT){
            $chat   = \App\Chat::find($payload->chat_id)->getAttributes();
            return [
                'type_id'   => \App\Trade\Log::LOG_TYPE_CHAT,
                'payload'   => $chat,
                // 'tradeData' => $trade
            ];
        }
    }
}
