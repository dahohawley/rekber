<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model{
    public $table = 'chat';
    protected $guarded = [];

    const LIST_ALL  = 0;
    const LISTBY_REF_CODE = 1;
    const LISTBY_REF_ID     = 2;

    public function loadList($id = null, $mode = self::LIST_ALL,$params = array()){
        $builder    = \DB::table($this->table);
        
        if($mode    != self::LIST_ALL){
            switch ($mode) {
                case self::LISTBY_REF_ID:
                    $builder->where('ref_id','=',$id);
                    break;
            }
        }

        if(isset($params['ref_code']) && !empty($params['ref_code'])){
            $builder->where('code','=',$params['ref_code']);
        }

        if(isset($params['order'])){
            $builder->orderBy($params['order']['column'],$params['order']['dir']);
        }

        if(isset($params['start'])){
            $builder->offset($params['start']);
            $builder->limit($params['length']);
        }
        
        if($list = $builder->get()){
            $rows   = array();

            foreach($list as $row){
                $rows[]     = (array)$row;
            }

            $result = array(
                'status'    => true,
                'data'      => $rows
            );
        }else{
            $result = array(
                'status'    => false,
                'data'      => array(),
                'info'      => "data not found"
            );
        }

        return $result;
    }
}
