<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Activity\Type;
use App\Activity\Template;

class Activity extends Model{
    const CREATED_AT = 'create_dtm';
    const UPDATED_AT = 'terminate_dtm';

    public $table   = 'activity';

    const LIST_ALL  = 0;
    const LISTBY_USER_ID_ONLY_ACTIVE = 1;
    
    public function create($user_id = null,$typeCode,$templateData = array(),$additional = array(),$show = true){
        $result         = new \stdClass;

        /* Load Template */
        $type           = new Type();
        $loadType       = $type->where('code','=',$typeCode)->first()->getAttributes();
        if(!$loadType){
            $result->code   = 1;
            $result->info   = "type code not found";
            $result->data   = $typeCode;
            return $result;
        }
        $template       = new Template();
        $loadTemplate   = $template->where('type_id','=',$loadType['type_id'])->first()->getAttributes();
        $template->fill($loadTemplate);
        $description    = $template->assignVar($templateData);

        $this->type_id  = $loadType['type_id'];
        $this->user_id  = $user_id;
        $this->description = $description;

        if($show){
            $this->terminate_dtm = null;
        }

        if($additional){
            $this->additional = json_encode($additional);
        }

        if($this->save()){
            $result->code   = 0;
            $result->info   = "success saving activity";
            $result->data   = $this->getAttributes();
        }else{
            $result->code   = 1;
            $result->info   = "Failed saving activity";
            $result->data   = $this->getAttributes();
        }
        
        return $result;
    }

    public function list($ids = null, $mode = self::LISTBY_USER_ID_ONLY_ACTIVE,$params = array()){
        $builder        = \DB::table($this->table);

        if($mode != self::LIST_ALL){
            switch($mode){
                case self::LISTBY_USER_ID_ONLY_ACTIVE:
                    $builder->where('user_id','=',$ids);
                    $builder->where("terminate_dtm");
            }
        }

        if(isset($params['order'])){
            /* TODO :: ORDER BY */
        }else{
            $builder->orderBy('create_dtm','desc');
        }

        if(isset($params['length'])){
            $builder->limit(intval($params['length']));
            $builder->offset(intval($params['start']));
        }
        
        $list           = $builder->get();
        $rows           = array();
        foreach($list as $row){
            $rows[]     = (array)($row);
        }

        return $rows;
    }
}
