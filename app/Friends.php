<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use \App\Friends\Status;

class Friends extends Model{
    protected $table                = 'friends';
    public $timestamps              = true;
    protected $guarded              = [];

    const LOADBY_ID                 = 0;
    const LOADBY_SUBJECT_ID         = 1;
    const LIST_ALL                  = 0;
    const LISTBY_SUBJECT_ID         = 1;
    const LISTBY_REF_ID             = 2;
    const LISTBY_OWNED_FRIENDS      = 3; /* cari yang ref_id / subject_idnya = user_id dia */
    const LISTBY_REQUEST_FRIENDS    = 4; /* cari yang ref_id / subject_idnya = user_id dia */



    /**
     * @param data diisi dengan data dari \User yang mau di cek, apakah sudah berteman dengan yang login?
     */
    public function isAfriend(array $data){
        $auth       = \Auth::user();
        $myUserId   = $auth->id;
        
        $load       = $this->loadRow($data['id'],self::LOADBY_SUBJECT_ID,array('ref_id' => $myUserId));
        if($load){
            /* TODO ::
            * Rubah hardcode 1 menjadi status_pertemanan
            */
            $data['is_friend'] = $load->status_id;
            $data['friend_data'] = $load;
        }else{
            $data['is_friend'] = 0;
        }

        return $data;
    }

    public function loadRow($id = null,$mode = self::LOADBY_ID, $params = array()){
        $builder    = \DB::table($this->table);
        switch($mode){
            case self::LOADBY_SUBJECT_ID:
                // $builder->where('subject_id','=',$id);
                $builder->where(function($nest) use(&$id){
                    $nest->where('subject_id','=',$id)
                    ->orWhere('ref_id','=',$id);
                });
                break;
            case self::LOADBY_ID:
                $builder->where('id','=',$id);
                break;
            default:
                $builder->where('id','=',$id);
                break;
        }

        if(isset($params['ref_id'])){
            $builder->where(function($nest) use(&$params){
                $nest->where('ref_id','=',$params['ref_id'])
                    ->orWhere('subject_id','=',$params['ref_id']);
            });
        }

        return $builder->first();
    }

    public function save($update = false){
        if($update){
            $update     = $this->where('id',$this->id)
            ->update(
                $this->getAttributes()
            );
            if($update){
                $id     = $this->id;
            }else{
                $id     = false;
            }
        }else{
            $id = \DB::table($this->table)->insertGetId(
                $this->getAttributes()
            );
        }

        return $id;
    }

    public function saveEx($data = array()){
        if(isset($data['id'])){

        }else{
            /* Cari dulu udah berteman atau belum */
            $load   = $this->loadRow(
                $data['subject_id'],
                self::LOADBY_SUBJECT_ID,
                $data
            );
            if($load){
                // Kalo berteman gausah ditambahin
                $res    = array(
                    'status'    => false,
                    'info'      => "Already friend"
                );
                return $res;
            }
            $data['created_at']     = Carbon::now();
            $this->fill($data);
            if($id  = $this->save(false)){
                $res    = array(
                    'status'    => true,
                    'info'      => "Success",
                    'data'      => $this->getAttributes()
                );
                /* Create activity */
                
            }else{
                $res    = array(
                    'status'    => false,
                    'info'      => "Failed saving friends",
                );
            }
        }

        return $res;
    }

    public function cancelRequest($friend_id = null){
        $delete     = $this->where('id','=',$friend_id)->delete();
        if($delete){
            $response   = array(
                'status'    => true
            );
        }else{
            $response   = array(
                'status'    => false
            );
        }
        return $response;
    }

    public function acceptRequest($friend_id = null){
        try{
            $update = $this->where('id',$friend_id)->update(array(
                'status_id' => Status::STATUS_ACCEPTED
            ));
            if($update){
                /* Create activity */
                $loadFriendData         = $this->where('id',$friend_id)->first()->getAttributes();
                $subject                = $loadFriendData['subject_id'];
                $ref_id                 = $loadFriendData['ref_id'];

                /* load subject Detail */
                $userDetail             = new \App\User\Detail();
                $dataDetail             = (array)$userDetail->loadRow($ref_id,\App\User\Detail::LOADBY_USER_ID);
                
                $activity               = new \App\Activity();
                $createSubjectActivity  = $activity->create($subject,"NEW_FRIENDS",array(
                    'profile_url'       => url('/user/profile?id='.$ref_id),
                    'friend_name'       => $dataDetail['first_name'].' '.$dataDetail['last_name']
                ));
                

                /* load user Detail */
                $userDetail             = new \App\User\Detail();
                $dataDetail             = (array)$userDetail->loadRow($subject,\App\User\Detail::LOADBY_USER_ID);
                
                $activity               = new \App\Activity();
                $createUserActivity  = $activity->create($ref_id,"NEW_FRIENDS",array(
                    'profile_url'       => url('/user/profile?id='.$subject),
                    'friend_name'       => $dataDetail['first_name'].' '.$dataDetail['last_name']
                ));

                return array(
                    'status'     => true,
                    'info'      => "success accepting"
                ); 

            }else{
                return array(
                    'status'     => false,
                    'info'      => "Something wrong"
                );
            }
        }catch(\Exception $ex){
            error_log("ERROR ::". $ex->getMessage().' File '.$ex->getFile().' Line : '.$ex->getLine());
            return array(
                'status'     => false,
                'info'      => "Something wrong"
            );
        }
    }

    public function list($id = null, $mode = self::LIST_ALL, $params = array()){
        $builder      = \DB::table($this->table);

        if(isset($params['start'])){
            $builder->offset(intval($params['start']));
        }

        if(isset($params['length'])){
            $builder->limit(intval($params['length']));
        }

        if($mode !== self::LIST_ALL){
            switch($mode){
                case self::LISTBY_OWNED_FRIENDS:
                    $builder->where(function($nest) use(&$id){
                        $nest->where('ref_id','=',$id)->orWhere('subject_id','=',$id);
                    });
                    $builder->where('status_id','=',Status::STATUS_ACCEPTED);
                break;
                case self::LISTBY_REQUEST_FRIENDS:
                    $builder->where('subject_id','=',$id);
                    $builder->where('status_id','=',Status::STATUS_REQUEST);
                break;
            }
        }

        if(isset($params['order'])){
            $order  = $params['order'];
            if(isset($order['column']) && isset($order['dir'])){
                $builder->orderBy($order['column'],$order['dir']);
            }
        }
        $fetchAll       = $builder->get();
        $rows           = [];
        $countFiltered  = 0;
        if($fetchAll){
            foreach($fetchAll as $row){
                $rows[]     = $row;
            }
            // $countFiltered  = $builder;
            // $countFiltered->select(\DB::raw("COUNT(*) as countFiltered"));
            // $countFiltered->limit(1)->offset(0);
            // $countFiltered  = $countFiltered->first()->countFiltered;
            $countFiltered      = count($rows);
        }

        $countAll       = $this->count();
        
        $result         = new \stdClass;
        $result->rows   = $rows;
        $result->countAll = $countAll;
        $result->countFiltered = $countFiltered;
        
        return $result;

    }

    public function countRequest($id = 0){
        $builder    = \DB::table($this->table);
        $builder->where('subject_id','=',$id);
        $builder->where('status_id','=',Status::STATUS_REQUEST);
        $builder->select(\DB::raw("COUNT(*) as countTotal"));
        
        if($fetch = $builder->first()){
            return $fetch->countTotal;
        }else{
            return 0;
        }

    }
}
