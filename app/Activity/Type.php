<?php

namespace App\Activity;

use Illuminate\Database\Eloquent\Model;



class Type extends Model{
    protected $table = 'activity_type';
    protected $primaryKey = 'type_id';
    public $timestamps = false;
    
}
