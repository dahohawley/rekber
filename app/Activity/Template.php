<?php

namespace App\Activity;

use Illuminate\Database\Eloquent\Model;

class Template extends Model{
    public $table = 'activity_template';
    protected $guarded = [];
    
    public function assignVar($variable = array()){
        $description        = $this->description;
        $newDescription = str_replace(array_keys($variable), array_values($variable), $description);
        $newDescription = str_replace("%","",$newDescription);
        return $newDescription;
    }
}
