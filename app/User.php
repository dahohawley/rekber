<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use function Ramsey\Uuid\v1;
use App\User\Status;
class User extends Authenticatable
{
    protected $table    = 'acc_users';

    use Notifiable;

    public $timestamps = true;

    protected $guarded = [
        'updated_at',
        'created_at'
    ];

    const LOADBY_ID     = 0;
    const LIST_ALL      = 0;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function saveEx($data = array()){
        if(isset($data['id'])){
            /* Update */
        }else{
            /* Create new */
            /* Check duplicate */
            $check = $this->whereUsername($data['username'])->orWhere('email',$data['email'])->first();
            if($check){
                /* Kalo duplikat usernya */
                if($check->email == $data['email']){
                    $info   = "Email already used!";
                }else{
                    $info   = "Username already used!";
                }
                $result = array(
                    'success'    => false,
                    'info'      => $info,
                );
                return $result;
            }
            $activationHash     = sha1(md5($data['email'].date('Y-m-d')));
            
            $this->username     = $data['username'];
            $this->password     = password_hash($data['password'],PASSWORD_BCRYPT);
            $this->email        = $data['email'];
            $this->status_id    = Status::STATUS_REGISTER;
            $this->token        = $activationHash;

            $to_name = $data['email'];
            $to_email = $data['email'];
            if($this->save()){
                /* Send Email */
                $activationUrl      = url('/activate?code='.$activationHash);

                $data = array(
                    "email" => $data['email'],
                    "activateUrl"   => $activationUrl
                );

                try{
                    $sendEmail = \Mail::send('emails.register', $data, function($message) use ($to_name, $to_email) {
                        $message->to($to_email, $to_name)
                        ->subject('[Rekber] Verification code ');
                        $message->from('dahohawley@gmail.com','[REKBER] Verification Code');
                    });

                    $result = array(
                        'success'   => true,
                    );
                }catch(\Exception $ex){
                    $result     = array(
                        'success'   => false,
                        'info'      => 'failed sending email'
                    );
                    /* Delete Saved user */
                    $this->where('id','=',$this->id)->delete();
                } 
            } else {
                $result     = array(
                    'success'   => false,
                    'info'      => 'failed saving user'
                );
            }
        }
        return $result;
    }
    /** 
     * @author Ramdhani Lukman
     * @param query query yang bakal di search
     * @param start offset
     * @param length jumlah data yang mau ditampilkan
     * @param mode baru satu sih, karena belum nemu case lain
     * @param removeConfidential kalo di set true, data yang confidential bakal dihilangkan dari row 
     * @return Object berisi data  
     */ 
    public function list($query = null,$start = 1,$length = 10,$mode = self::LIST_ALL,$removeConfidential = true){
        $users      = $this->join('acc_user_status AS aus','acc_users.status_id' ,'=', 'aus.id')
                            ->join('acc_users_detail AS aud','acc_users.id' ,'=', 'aud.user_id')
                            ->select('acc_users.*','aus.name AS status_name','aus.id AS status_id',\DB::raw("CONCAT(first_name,' ',last_name) as full_name"));

        if($query){
            $users->where(function($nest) use(&$query){
                $nest->where('acc_users.username','LIKE',"%$query%")
                ->orWhere('acc_users.email','LIKE',"%$query%")
                ->orWhere(\DB::raw('CONCAT(first_name," ",last_name)'),'LIKE',"%$query%");
            });
        }

        if($mode != self::LIST_ALL){
            /* mode */
        }

        $fetchAll   = $users->get();
        $rows       = array();
        if($fetchAll){
            foreach($fetchAll as $user){
                if(!$removeConfidential){
                    $rows[]     = $user->getAttributes();
                }else{
                    $rows[]     = $this->removeConfidential($user->getAttributes());
                }
            }
        }

        /* Hitung total row berdasarkan kondisi2nya */
        $countFiltered  = $users;
        $countFiltered->select(\DB::raw("COUNT(*) as countFiltered"));
        $countFiltered  = $countFiltered->first()->countFiltered;

        $result     = new \stdClass;
        $result->rows = $rows;
        $result->countFiltered = $countFiltered;
        $result->countAll       = $this->count();
        return $result;
    }

    public static function removeConfidential(array $data){
        unset(
            $data['password'],
            $data['created_at'],
            $data['updated_at'],
            $data['token'],
            $data['remember_token'],
            $data['attempts']
        );
        return $data;
    }

    public function loadRow($id = null, $mode = self::LOADBY_ID){
        $builder    = \DB::table('acc_users AS au');
        $builder->join(
            'acc_users_detail AS aud',
            'au.id','=','aud.user_id'
        );
        switch ($mode) {
            case self::LOADBY_ID:
                $builder->where('au.id','=',$id);
            break;
            
            default:
                $builder->where('au.id','=',$id);
            break;
        }
        
        $fetch      = (array)$builder->first();
        $this->fill($fetch);
        return $fetch;
    }
    
}
