<?php

namespace App\Http\Middleware;

use App\User\Detail;
use Closure;

class CheckDetail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        $user           = \Auth::user();
        $userDetail     = new Detail();
        $load           = $userDetail->whereUserId($user->id)->first();
        if(!$load){
            return redirect()->route('createProfile')->with('error',"Please complete your profile first!");
        }else{
            return $next($request);
        }
    }
}
