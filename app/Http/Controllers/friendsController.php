<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Friends as friendsModel;
use App\Friends\Status;

class FriendsController extends Controller{
    public function index(Request $req){
        $user   = \Auth::user();
        $ref_id = $user->id;

        $friendsModel   = new friendsModel();
        $count = $friendsModel->countRequest($ref_id);
        
        return view('friends.index',array(
            'ref_id'    => $ref_id,
            'friend_request' => $count
        ));
    }
    public function find(Request $request){
        $query      = $request->query();

        // Load user
        $user       = new User();
        $data       = $user->list($query['query'],1,5,$user::LIST_ALL);

        /* Remove self from list */
        $user       = \Auth::user();
        if($data->countFiltered){
            $rows   = $data->rows;
            foreach($rows as $key => $row){
                if($row['id'] == $user->id){
                    unset($rows[$key]);
                    $data->countFiltered = $data->countFiltered - 1;
                    break;
                }
            }
            $data->rows     = $rows;
        }


        /* Cari udah menjadi teman atau belum */
        if($data->countFiltered){
            $rows       = $data->rows;
            $newRow     = array();
            foreach($rows as $row){ 
                $friends    = new \App\Friends();
                $newRow[]   = $friends->isAfriend($row);
            }
            $data->rows   = $newRow;
        }
        
        $res        = array(
            'status'    => true,
            'data'      => $data
        );

        return response()->json($res    );
    }
    public function add(Request $request){
        $post               = $request->post();
        $user               = \Auth::user();
        $post['status_id']  = Status::STATUS_REQUEST; /* Karena awal request */
        $post['ref_id']     = $user->id;
        $friends    = new friendsModel();
        $result     = $friends->saveEx($post);
        return response()->json($result);
    }
    public function cancel(Request $request){
        $post   = $request->post();
        $friends = new friendsModel();
        $result = $friends->cancelRequest($post['friend_id']);
        return response()->json($result);
    }
    public function accept(Request $request){
        $post   = $request->post();
        $friends = new friendsModel();
        $result = $friends->acceptRequest($post['friend_id']);
        return response()->json($result);
    }
    public function list(Request $request){
        $params     = $request->query();
        if(isset($params['ref_id'])){
            if(isset($params['status_id'])){
                if($params['status_id'] == Status::STATUS_REQUEST){
                    $mode   = friendsModel::LISTBY_REQUEST_FRIENDS;
                }else if($params['status_id'] == Status::STATUS_ACCEPTED){
                    $mode   = friendsModel::LISTBY_OWNED_FRIENDS;
                }
            }
            $ids    = $params['ref_id'];
        }else{
            $mode   = friendsModel::LIST_ALL;
            $ids    = null;
        }
        
        $friends    = new friendsModel();
        $result     = $friends->list($ids,$mode,$params);

        /* Cari subject name nya */
        $rows       = $result->rows;
        $sessUser   = \Auth::user();
        $myUserId   = $sessUser->id;
        foreach($rows as $key => $row){
            if($row->subject_id == $myUserId){
                $findUserId     = $row->ref_id;
            }else{
                $findUserId     = $row->subject_id;
            }
            $user           = new \App\User();
            $loadSubject    = $user->loadRow($findUserId);
            $subjectData    = $user->removeConfidential($loadSubject);
            $rows[$key]->subject_data = $subjectData;
        }

        return response()->json($result);
    }
    public function countTotalRequest(Request $request){
        $user   = \Auth::user();
        $user_id = $user->id;

        $friends        = new friendsModel();
        $totalRequest   = $friends->countRequest($user_id);
        
        return response()->json(array(
            'success'   => true,
            'data'      => $totalRequest
        ));
    }
}