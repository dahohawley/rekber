<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class activityController extends Controller{
    public function list(Request $request){
        $activity       = new \App\Activity();
        $data           = $activity->list($request->query('user_id'),\App\Activity::LISTBY_USER_ID_ONLY_ACTIVE,$request->query());
        return response()->json($data);
    }
}
