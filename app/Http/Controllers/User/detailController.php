<?php
namespace App\Http\Controllers\User;

use \App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\User\Detail;
use \App\User;

class DetailController extends Controller{
    
    public function index(Request $request){
        $user       = \Auth::user();
        if($request->query('id')){
            $userId     = $request->query('id');
            $friends    = new \App\Friends;
            if($request->query('id') == $user->id){
                $isMyProfile = true;
                $isAFriends = true;
                $userId     = $user->id;
            }else{
                $isAFriends = $friends->isAfriend(array('id' => $userId))['is_friend'];
                $isMyProfile = false;
            }
        }else{
            $isMyProfile = true;
            $isAFriends = true;
            $userId     = $user->id;
        }

        /* Checking grant */
        $grantEdit      = false;
        if($userId == $user->id ){
            $grantEdit  = true;
        }

        
        $detail     = new Detail();
        $loadDetail = (array)$detail->loadRow($userId,Detail::LOADBY_USER_ID);
        if(!$loadDetail){
            if($grantEdit){
                return redirect()->route('createProfile')->with('error',"Please complete your profile first!");
            }else{
                return view('errors.404');
            }
        }

        
        
        /* Load user */
        $userModel       = new User();
        $loadUser   = $user->loadRow($userId);
        $loadUser   = $user->removeConfidential($loadUser);

        return view('user.detail.profile',array(
            'detail'    => $loadDetail,
            'user'      => $loadUser,
            'is_friend' => $isAFriends,
            'my_profile' => $isMyProfile,
        ));
    }

    public function create(Request $request){
        $auth   = \Auth::user();
        $user_id = $auth->id;

        $post               = $request->input();
        $post['user_id']    = $user_id;
        $userDetail = new \App\User\Detail();
        if(isset($post['id']) && !empty($post['id'])){

        }else{
            /* insert */
            $userDetail->fill($post);
            if($userDetail->save()){
                $result     = array(
                    'success'   => true,
                    'data'      => $userDetail->getAttributes(),
                );
            }else{
                $result     = array(
                    'success'   => false,
                    'info'      => "Failed saving user detail"
                );
            }
        }
        
        return response()->json($result);
        
    }
}