<?php
namespace App\Http\Controllers;

use App\Events\TradeActivity;
use Illuminate\Http\Request;
use \App\Trade as Trade;
use \App\Trade\Status as TradeStatus;
use \App\User\Detail as UserDetail;
use \App\Trade\Log;
use \Auth;

class TradeController extends Controller{

    public function index_old(Request $req){
        $user_id = \Auth::id();
        return view('trade.index',array(
            'user_id'   => $user_id
        ));
    }

    public function index(){
        $userId     = Auth::id();
        return view('trade.transaction',array('user_id' => $userId));
    }

    public function request(Request $request){
        $user               = \Auth::user();
        $requester_id       = $user->id;

        $subject_id         = $request->id;

        /* Check harus berteman dulu */
        $friends            = new \App\Friends();
        $userModel          = new \App\User;
        $userModel->loadRow($subject_id);
        if(!$userModel->getAttributes()){
            // sekalian user targetnya ada ga
            return redirect()->route('tradeIndex')->with('error','The person you requested to trade is not found');
        }
        $checkUserData          = $userModel->getAttributes();
        $checkUserData['id']    = $checkUserData['user_id'];

        $friendCheck        = $friends->isAfriend($checkUserData);
        
        if($friendCheck['is_friend'] != 2){
            // kalo bukan temen, masuk kesini bro
            return redirect()->route('tradeIndex')->with('error','You need to add this person as friend before requesting trade');
        }


        /* check dulu subjectnya udah punya detail atau belum */
        $userDetail         = new UserDetail();
        $loadSubjectDetail  = (array)$userDetail->loadRow($subject_id,UserDetail::LOADBY_USER_ID);
        if(!$loadSubjectDetail){
            return redirect()->route('tradeIndex')->with('error','cannot trade with this person. this subject is not completing profile detail yet.');
        }

        


        /* check masih ada transaksi yang status request ga */
        $tradeRequest       = new Trade();
        $loadActiveRequest  = $tradeRequest->loadRow($requester_id,Trade::LOADBY_REQUESTED,array('subject_id' => $subject_id));

        if($loadActiveRequest){
            return redirect()->route('tradeIndex')->with('error',array(
                'info'      => 'You already have unapproved trade request with '.$loadSubjectDetail['first_name'].' '.$loadSubjectDetail['last_name'],
                'data'      => array(
                    'request'   => $loadActiveRequest,
                ),
                'handler'   => 'waw'
            ));
        }
        
        $tradeRequest                   = new Trade();
        $tradeRequest->requester_id     = $requester_id;
        $tradeRequest->subject_id       = $subject_id;
        $tradeRequest->trade_status     = TradeStatus::STATUS_REQUEST;

        if($tradeRequest->save()){    
            return redirect()->route("tradeIndex");
        }else{
            return redirect('homepage')->with('error','error requesting trade');
        }
    }

    public function list(Request $request){
        $params     = $request->query();
        $user       = \Auth::user();
        $userId     = $user->id;

        $mode       = (isset($params['status_id']) && !empty($params['status_id'])) ? intval($params['status_id']) : Trade::LISTBY_OWNED;

        $trade      = new Trade();
        $listTrade  = $trade->loadList($userId,$mode,$params,true);
        if($listTrade){
            return response()->json(array(
                'status'    => true,
                'data'      => $listTrade
            ));
        }else{
            return response()->json(array(
                'status'    => false,
                'data'      => null
            ));
        }
    }

    public function tradePage($tradeId){
        $userId     = Auth::id();
        $trade      = new Trade();
        
        $trade->loadRow($tradeId);
        $dataTrade = $trade->getAttributes();
        if(!$dataTrade){
            return redirect()->route('tradeIndex')->with('error',"Trade not exists");
        }
        /* Validasi, trade yang bisa dibuka hanya trade yang sudah di accept */
        if($dataTrade['trade_status'] !== TradeStatus::STATUS_ACCEPTED){
            return redirect()->route('tradeIndex')->with('error',"This transaction is not accepted yet");
        }

        $isParticipant = $trade->isParticipant($userId);
        if(!$isParticipant['status']){
            return redirect()->route('tradeIndex')->with('error',$isParticipant['info']);
        }

        return view('trade.transaction');
    }
    
    /** 
     * untuk merubah status request
     * bisa untuk cancel, accept atau decline
     */
    public function tradeResponse(Request $request){
        $post       = $request->post();
        $userId     = \Auth::id();

        /* pastikan partisipan */
        $tradeId        = $request->trade_id;
        if(!isset($tradeId)){
            return response()->json(array(
                'status'    => false,
                'info'      => 'Missing parameter'
            ));
        }

        $trade          = new Trade();
        $trade->loadRow($tradeId);
        if(!$trade->getAttributes()){
            return response()->json(array(
                'status'    => false,
                'info'      => 'trade not found'
            ));
        }

        $tradeData      = $trade->getAttributes();
        $isParticipant  = $trade->isParticipant();
        if(!$isParticipant['status']){
            return response()->json(array(
                'status'    => false,
                'info'      => 'Youre not participant of this transaction'
            ));
        }

        /* proses perubahan data check dulu, accept, cancel, atau deny*/
        if(
            $post['status'] == TradeStatus::STATUS_ACCEPTED || 
            $post['status'] == TradeStatus::STATUS_REJECTED
        ){
            /* Validasi hanya status request yang dapat di update */
            if($tradeData['trade_status'] != TradeStatus::STATUS_REQUEST){
                return response()->json(array(
                    'status'    => false,
                    'info'      => 'This transaction status cannot be changed'
                ));
            }
            /* Validasi yang nge accept harus subject */
            if($tradeData['subject_id'] !== $userId){
                return response()->json(array(
                    'status'    => false,
                    'info'      => 'Youre have no right to accept this request'
                ));
            }

            /* kalo berhasil validasi, berubah statusnya jadi accepted */
            $trade->trade_status = $post['status'];
            $trade->where('trade_id',$tradeData['trade_id'])->update($trade->getAttributes());
            return response()->json(array(
                'status'    => true,
                'info'      => 'success responding trade'
            ));
        }else if($post['status'] == TradeStatus::STATUS_CANCELED){
            /* Validasi hanya status request yang dapat di update */
            if($tradeData['trade_status'] != TradeStatus::STATUS_REQUEST){
                return response()->json(array(
                    'status'    => false,
                    'info'      => 'This transaction status cannot be changed'
                ));
            }
            /* Validasi yang nge accept harus subject */
            if($tradeData['requester_id'] !== $userId){
                return response()->json(array(
                    'status'    => false,
                    'info'      => 'Youre have no right to accept this request'
                ));
            }

            /* kalo berhasil validasi, berubah statusnya jadi accepted */
            $trade->trade_status = $post['status'];
            $trade->where('trade_id',$tradeData['trade_id'])->update($trade->getAttributes());
            return response()->json(array(
                'status'    => true,
                'info'      => 'success responding trade'
            ));
        }
    }

    /**
     * 
     */
    public function sendChat(Request $request){
        $trade  = new Trade();
        $isParticipant = $trade->isParticipant(Auth::id(),null,$request->tId);

        if(!$isParticipant['status']){
            return response()->json($isParticipant);
        }

        // masukin ke chat
        $chat           = new \App\Chat();
        $chat->code     = "TRD";
        $chat->ref_id   = $request->tId;
        $chat->message  = $request->message;
        $chat->created_by = Auth::id();
        $saveChat      = $chat->save();

        // kalo gagal simpeen chat
        if(!$saveChat){
            return response()->json(array(
                'status'    => false,
                'info'      => "failed saving chat"
            ));
        }

        // Create Log transaksi
        $log                = new Log();
        $log->type_id       = 3; /* log type chat */
        $log->trade_Id      = $request->tId;
        $log->data          = json_encode(array('chat_id' => $chat->id));
        if(!$log->save()){
            $chat->delete();
            return response()->json(array(
                'status'    => false,
                'info'      => "failed saving chat log"
            ));
        }

        $event = event(new TradeActivity($log));

        // return sukses
        return response()->json(array(
            'status'    => true,
            'info'      => "success saving chat",
            'data'      => $chat->getAttributes()
        ));
    }

    public function loadChat(Request $request){
        $userId     = Auth::id();
        $trade      = new Trade();
        $isParticipant = $trade->isParticipant($userId,null,$request->tId);
        if(!$isParticipant){
            return response()->json($isParticipant);
        }

        $chat           = new \App\Chat();
        $result         = $chat->loadList($request->tId,\App\Chat::LISTBY_REF_ID,array('ref_code' => "TRD"));
        return response()->json($result);
        
    }

    public function testPusher(Request $request){
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => false
        );
        $pusher = new \Pusher\Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $data['message'] = 'anjing';
        $pusher->trigger('my-channel', 'my-event', $data);

        return response()->json(['code' => 0,'info' => 'Pusher test']);
    }
}