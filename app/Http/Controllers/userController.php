<?php
namespace App\Http\Controllers;

use App\User as AppUser;
use App\User\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller{
    public function register(){
        return view('user.register');
    }
    public function create(Request $request){
        $post       = $request->post();
        $userModel  = new AppUser();
        $result     = $userModel->saveEx($post);
        
        return response()->json($result);
    }
    public function activate(Request $request){
        if(!$request->query('code')){
            return view(
                'user.activation.invalid',
                array(
                    'info'      => "Invalid registration code"
                )
            );
        }
        
        $code       = $request->query('code');
        $loadCode   = \App\User::whereToken($code)->first();
        if($loadCode){
            $user   = $loadCode->getAttributes();

            /* cek dulu statusnya masih register atau bukan */
            if($user['status_id'] !== \App\User\Status::STATUS_REGISTER){
                /* Get Status */
                $loadStatus         = \App\User\Status::whereId($user['status_id'])->first();
                $status             = $loadStatus->getAttributes();

                return view(
                    'user.activation.invalid',
                    array(
                        'info'      => "User is ".$status['name']
                    )
                );
            }
            
            $user['status_id']  = \App\User\Status::STATUS_ACTIVE;
            $user['updated_at'] = Carbon::now();
            $mUser              = new \App\User();
            $update             = $mUser->where('id','=',$user['id'])->update($user); 
            /* set session */
            $mUser->fill($user);
            $loggedIn           = \Auth::loginUsingId($mUser->id);
            
            if($loggedIn){
                return redirect()->route('profile');
            }else{
                return redirect()->route('login')->with('message',"Please login to continue");
            }
            
        }else{
            return view('user.activation.invalid',array(
                'info'  => 'Invalid code!'
            ));
        }
    }
    public function login(Request $request){
        $user       = new \App\User();

        $loadUser = $user->where('email',$request->input('username'))->
        orWhere('username',$request->input('username'))->first();

        if(!$loadUser){
            $result     = array(
                'success'   => false,
                'info'      => "Username / email is not registered"
            );
            return redirect()->back()->with('message',"Username \ Email is not registered");
        }else{
            $password   = $request->input('password');

            /* verifikasi password */
            if(password_verify($password,$loadUser->password)){
                if($loadUser->status_id != Status::STATUS_ACTIVE){
                    $result         = array(
                        'status'   => 0,
                        'info'      => "This account is not active / locked / banned"
                    );
                    return redirect()->back()->with('message',"This account is not active / locked / banned");
                }else{
                    $result         = array(
                        'status'   => 1
                    );
                    \Auth::loginUsingId($loadUser->id);
                }
            }else{
                /* Tambahin attempt */
                $loadUser->attempts         = $loadUser->attempts + 1;
                $updateData                 = $loadUser->getAttributes();
                if($loadUser->attempts >= env('MAX_LOGIN_ATTEMPTS')){
                    $updateData['status_id'] = \App\User\Status::STATUS_LOCKED;
                }
                $user->where('id',$loadUser->id)->update($updateData);
                
                $result         = array(
                    'status'   => 0,
                    'info'      => "Invalid password"
                );
                return redirect()->back()->with('message',"Invalid password");

            }
        }
        return redirect()->route('home');
    }
    public function list(Request $request){
        $user       = new AppUser();
        $mode       = $user::LIST_ALL;
        $params     = $request->query();
        
        $length     = (isset($params['length']) && !empty($params['length'])) ? $params['length'] : 1;
        $start      = (isset($params['start']) && !empty($params['start'])) ? $params['start'] : 999999;
        $query     = (isset($params['query']) && !empty($params['query'])) ? $params['query'] : null;

        $result     = $user->list($query,$start,$length,$mode);

        return response()->json($result);
    }
    
}