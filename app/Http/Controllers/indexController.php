<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class IndexController extends Controller{
    public function index(){
        $user   = \Auth::user();
        if($user){
            return view('index.index');
        }else{
            return view('layouts.outside');
        }
    }
}