<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification\Token;
class NotificationController extends Controller{

    public function saveToken(Request $request){
        $post       = $request->input();
        
        /* Load dulu berdasarkan userID */
        $user       = \Auth::user();
        if(!$user){
            $result['status'] = false;
            $result['info']   = "Unauthorized";
            return response()->json($result,401);
        }

        $user_id    = $user->id;
        $loadToken  = Token::whereUserId($user_id)->first();
        if($loadToken){
            /* Update token */
            $token      = new Token();
            $update     = $token->where('user_id',$user_id)->update(array('token' => $post['token']));
            
            if($update){
                $result['status']   = true;
                $result['data']     = $loadToken->id;
            }else{
                $result['status']   = false;
                $result['info']     = "failed updating token";
            }
              
        }else{
            /* Create new Token */
            $token      = new Token();
            $token->fill(array(
                'user_id'   => $user_id,
                'token'     => $post['token'] 
            ));
            if($token->save()){
                $result['status']   = true;
                $result['data']     = $token->id;
            }else{
                $result['status']   = false;
                $result['info']     = "failed saving token";
            }
        }
        return response()->json($result);
    }
}
