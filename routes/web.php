<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# Symbol
    Route::get('/', 'indexController@index')->name('homepage')->middleware('guest');
# A
    Route::get('/activate', 'userController@activate');

    Route::get('/activity','activityController@list')->middleware('auth')->name('getActivity');
# B
# C
# D
# E
# F
    Route::get('/friends','friendsController@index')->middleware('auth')->name('friends');
    Route::get('/friends/total-request','friendsController@countTotalRequest')->middleware('auth')->name('countTotalFriendRequest');
    Route::get('/friends/find','friendsController@find')->middleware('auth')->name('findFriends');
    Route::get('/friends/list','friendsController@list')->middleware('auth')->name('listFriends');
    
    Route::post('/friends','friendsController@add')->middleware('auth')->name('addFriends');
    Route::post('/friends/cancel','friendsController@cancel')->middleware('auth')->name('cancelFriendRequest');
    Route::POST('/friends/accept','friendsController@accept')->middleware('auth')->name('acceptFriendRequest');

# G
# H
    Route::view('/home','index.index')->middleware('auth','checkDetail')->name('home');

# I
# J
# K
# L
    Route::view('/login','user.login')->middleware('guest')->name('login');
    Route::post('/login', 'userController@login')->middleware('guest')->name('loginAction');

    Route::get('/logout', function(){
        \Auth::logout();
        return redirect()->route('homepage');
    })->middleware('auth')->name('logout');
# M
# N 
    Route::post('/notification/save-token','NotificationController@saveToken')->middleware('auth')->name('saveToken');

# O
# P
# Q
# R
    Route::post('/register', 'userController@create')->middleware('guest');
    Route::get('/register', 'userController@register')->middleware('guest');

# S
# T
    Route::get('/trade','tradeController@index')->middleware('auth','checkDetail')->name('tradeIndex');
    Route::get('/trade/list','tradeController@list')->middleware('auth','checkDetail')->name('tradeList');
    Route::get('/trade/request','tradeController@request')->middleware('auth','checkDetail')->name('tradeRequest');
    Route::put('/trade/request','tradeController@tradeResponse')->middleware('auth','checkDetail')->name('tradeResponse');
    Route::post('/trade/send-chat','tradeController@sendChat')->middleware('auth','checkDetail')->name('tradeSendChat');
    Route::get('/trade/load-chat','tradeController@loadChat')->middleware('auth','checkDetail')->name('tradeLoadChat');
    
    Route::get('/test-pusher','tradeController@testPusher');

    // ini biarin paling bawah trade, karena ada {id} nya
    Route::get('/trade/{id}','tradeController@tradePage')->middleware('auth','checkDetail')->name('tradePage');
    
# U
    Route::get('/user/profile', 'User\detailController@index')->middleware('auth','checkDetail')->name('profile');
    Route::view('/user/profile/detail', 'user.detail.create')->middleware('auth')->name('createProfile');
    Route::post('/user/detail', 'User\detailController@create')->middleware('auth')->name('saveProfile');
    Route::get('/user/list','userController@list')->middleware('auth')->name('listUser');
# V
# W
# X 
# Y
# Z















