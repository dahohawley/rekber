$.fn.buttonLoader = function(action = 'hide' ,text = null){
    if(action == 'show'){
        
        if(text == null){
            defaultText     = 'Saving....';
        }else{
            defaultText     = text;
        }
        $(this).attr('disabled','true');
        $(this).html(defaultText+ ' <span class="fa fa-circle-notch fa-spin"></span>');

    }else if(action == 'hide'){

        if(text == null){
            defaultText     = 'Save';
        }else{
            defaultText     = text;
        }

        $(this).removeAttr('disabled','true');
        $(this).html(defaultText);
    }

    return $(this);

}

$.ajaxSetup({
    error   : function(jqXHR){
        console.log(jqXHR)
    }
})