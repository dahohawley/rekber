// TODO: Replace the following with your app's Firebase project configuration
var token;
var firebaseConfig = {
    apiKey: "AIzaSyCcmu0fb-v08vkTa4Wm0-Hx9LeP-IBJCyQ",
    authDomain: "rekber-dc031.firebaseapp.com",
    databaseURL: "https://rekber-dc031.firebaseio.com",
    projectId: "rekber-dc031",
    storageBucket: "rekber-dc031.appspot.com",
    messagingSenderId: "393989976521",
    appId: "1:393989976521:web:e37ad5a286f9e8612df818",
    measurementId: "G-45RCQSFTPG"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();
messaging.usePublicVapidKey("BF93DS1ZpXKTNiO3v2NnbW7DdpDTEj4SieDQU3W8TH0DHooZX-LoKVU9IXirIzfq3sZg-RlqHvkkuDUjk2zftd4");
messaging.requestPermission().then(function() {
    getToken();
});
messaging.onMessage((payload) => {
    console.log(payload);
});
function getToken(){
    messaging.getToken().then(function(currentToken) {
        if (currentToken) {
            token   = currentToken;
            saveToken(currentToken);
        } else {
            requestPermission();
        }
    }).catch(function(err) {
        console.log('Firebase :: An error occurred while retrieving token. ', err);
    });
}
function saveToken(newToken){
    $.ajax({
        url         : appBaseUrl+"/notification/save-token",
        type        : "POST",
        data        : {
            token   : newToken
        },
        headers     : {
            "X-CSRF-TOKEN" : csrf_token
        }
    });
}
