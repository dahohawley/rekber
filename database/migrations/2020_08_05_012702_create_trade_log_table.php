<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradeLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_log', function (Blueprint $table) {
            $table->id();
            $table->foreignId('type_id')->nullable(false);
            $table->integer('trade_id')->nullable(false);
            $table->longText('description');
            $table->longText('data')->comment("diisi oleh data json");
            $table->foreign('type_id')->references('id')->on('trade_log_type');
            $table->foreign('trade_id')->references('trade_id')->on('trade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_log');
    }
}
