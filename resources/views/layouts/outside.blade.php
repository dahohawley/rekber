<!DOCTYPE html>
<html lang="en">
  <head>
    <title>RekberAja By IziTechno</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('libraries/assets/css/animate.css')}} ">
    
    <link rel="stylesheet" href="{{ asset('libraries/assets/css/owl.carousel.min.css')}} ">
    <link rel="stylesheet" href="{{ asset('libraries/assets/css/owl.theme.default.min.css')}} ">
    <link rel="stylesheet" href="{{ asset('libraries/assets/css/magnific-popup.css')}} ">
    
    <link rel="stylesheet" href="{{ asset('libraries/assets/css/flaticon.css')}} ">
    <link rel="stylesheet" href="{{ asset('libraries/assets/css/style.css')}} ">
  </head>
  <body>
    
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="{{route('homepage')}}">Rekber Aja</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="{{route('homepage')}}" class="nav-link">Home</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Promo</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Blog</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Kontak</a></li>
            <li class="nav-item cta"><a href="{{url('/login')}}" class="nav-link"><span class="fa fa-sign-in"> </span> Masuk</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    <div class="hero-wrap js-fullheight">
      <div class="overlay"></div>
      <div class="container-fluid px-0">
        <div class="row d-md-flex no-gutters slider-text align-items-center js-fullheight justify-content-start">
          <img class="one-third js-fullheight align-self-end order-md-first img-fluid" src="{{ asset('libraries/assets/images/undraw_co-working_825n.svg')}}" alt="">
          <div class="one-forth d-flex align-items-center ftco-animate js-fullheight">
            <div class="text mt-5">
              <span class="subheading">Rekening Bersama</span>
              <h1 class="mb-3"><span>Buat Rekber,</span> <span>Transfer,</span> <span>Transaksi</span></h1>
              <p>Rekening Bersama adalah Jasa perantara antara Penjual dan Pembeli yang tidak dapat melakukan Transaksi dengan Bertemu langsung ( COD )</p>
              <p><a href="{{url('/register')}}" class="btn btn-secondary px-4 py-3">Ayo Mulai</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2>PROMO REKBERAJA</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn btn-primary btn-sm pull-right">Semua Promo</button>
          </div>
          <br><br>  
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url('{{ asset('libraries/assets/images/promo1.jpg') }}');">
              </a>
              <div class="text d-flex py-4">
                <div class="meta mb-3">
                  <div><a href="#">May 17, 2020</a></div>
                </div>
                <div class="desc pl-3">
                  <h3 class="heading"><a href="#">Potongan Diskon Fee Sebesar Rp. 25000</a></h3>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry" data-aos-delay="100">
              <a href="blog-single.html" class="block-20" style="background-image: url('{{ asset('libraries/assets/images/promo2.jpg') }}');">
              </a>
              <div class="text d-flex py-4">
                <div class="meta mb-3">
                  <div><a href="#">May 17, 2020</a></div>
                </div>
                <div class="desc pl-3">
                  <h3 class="heading"><a href="#">Potongan Diskon Fee Sebesar Rp. 25000</a></h3>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry" data-aos-delay="200">
              <a href="blog-single.html" class="block-20" style="background-image: url('{{ asset('libraries/assets/images/promo3.png') }}');">
              </a>
              <div class="text d-flex py-4">
                <div class="meta mb-3">
                  <div><a href="#">May 17, 2020</a></div>
                </div>
                <div class="desc pl-3">
                  <h3 class="heading"><a href="#">Potongan Diskon Fee Sebesar Rp. 25000</a></h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section services-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-3">FITUR UNGGULAN REKBERAJA</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-flex">
              <div class="icon d-flex align-items-center justify-content-center">
                <span class="flaticon-cloud"></span>
              </div>
              <div class="media-body pl-4">
                <h3 class="heading">Server</h3>
                <p class="mb-0">Web kami online 24 jam dan Dan pengecekan mengikuti jam online bank.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-flex">
              <div class="icon d-flex align-items-center justify-content-center">
                <span class="flaticon-server"></span>
              </div>
              <div class="media-body pl-4">
                <h3 class="heading">Pelayanan Cepat</h3>
                <p class="mb-0">Robot yang akan Memverifikasi Pembayaran sehingga Proses akan sangat cepat.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-flex">
              <div class="icon d-flex align-items-center justify-content-center">
                <span class="flaticon-customer-service"></span>
              </div>
              <div class="media-body pl-4">
                <h3 class="heading">Fee Murah</h3>
                <p>RekberAja Menawarkan Fee yang jauh lebih murah dibandingkan dengan yang lain.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-flex">
              <div class="icon d-flex align-items-center justify-content-center">
                <span class="flaticon-life-insurance"></span>
              </div>
              <div class="media-body pl-4">
                <h3 class="heading">UI</h3>
                <p>Kami membuat sebaik mungkin hingga pengguna dapat dengan mudah menggunakannya .</p>
              </div>
            </div>      
          </div>
          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-flex">
              <div class="icon d-flex align-items-center justify-content-center">
                <span class="flaticon-cloud-computing"></span>
              </div>
              <div class="media-body pl-4">
                <h3 class="heading">Data</h3>
                <p>List Transaksi selalu tersimpan untuk memudahkan pengguna mengambil informasi.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-6 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-flex">
              <div class="icon d-flex align-items-center justify-content-center">
                <span class="flaticon-settings"></span>
              </div>
              <div class="media-body pl-4">
                <h3 class="heading">Pembayaran</h3>
                <p>Kami Menerima Semua Jenis Pembayaran Seperti BCA,Mandiri,BRI,OVO,Gopay dll</p>
              </div>
            </div>      
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-3">Rekber Kami Melayani Pembayaran</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div style="text-align:center;padding-top:20px" class="pull-center">
            <img src="https://cekmutasi.co.id/images/supported-services/bank_bca.png" style="height:100%;max-height:50px;margin:10px 20px">
            <img src="https://cekmutasi.co.id/images/supported-services/ovo.png" style="height:100%;max-height:50px;margin:10px 20px">
            <img src="https://cekmutasi.co.id/images/supported-services/gopay.png" style="height:100%;max-height:50px;margin:10px 20px">
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-counter img" id="section-counter">
      <div class="container pb-md-5">
        <div class="row justify-content-center mb-5">
          <div class="col-md-8 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-3">Our Performance</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                  <div class="text">
                    <strong class="number" data-number="100">0</strong>
                    <span>Transaksi Selesai</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                  <div class="text">
                    <strong class="number" data-number="52">0</strong>
                    <span>Pengguna</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-no-pt ftco-no-pb bg-light">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-10">
            <div class="intro">
              <div class="row">
                <div class="col-md-8">
                  <h3>Punya Pertanyaan Tentang Kami?</h3>
                  <p>Kirimkan Pertanyaan jika ada yang membingungkan.</p>
                </div>
                <div class="col-md-4 d-flex align-items-center justify-content-center">
                  <a href="#" class="btn btn-tertiary px-4 py-3">Klik Disini</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-no-pb bg-light ftco-faqs">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <img src="{{ asset('libraries/assets/images/faq.png') }}" alt="" width="100%">
          </div>
          <div class="col-lg-6 pl-lg-5">
            <div class="heading-section mb-5 mt-5 mt-lg-0">
              <!-- <span class="subheading">FAQs</span> -->
              <h2 class="mb-3">Pertanyaan Terkait</h2>
              <p>Berikut adalah pertanyaan yang sering ditanyakan.</p>
            </div>
            <div id="accordion" class="myaccordion w-100" aria-multiselectable="true">
              <div class="card">
                <div class="card-header p-0" id="headingOne">
                  <h2 class="mb-0">
                    <button href="#collapseOne" class="d-flex py-3 px-4 align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">
                      <p class="mb-0">Cara Membuat Akun ?</p>
                      <i class="fa" aria-hidden="true"></i>
                    </button>
                  </h2>
                </div>
                <div class="collapse show" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">
                  <div class="card-body py-3 px-0">
                    <ol>
                      <li>Lakukan Pendaftaran Pada Tombol di atas.</li>
                      <li>Isikan Form dengan benar</li>
                      <li>Konfirmasi Email</li>
                      <li>Rekber Sudah bisa dilakukan</li>
                    </ol>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header p-0" id="headingTwo" role="tab">
                  <h2 class="mb-0">
                    <button href="#collapseTwo" class="d-flex py-3 px-4 align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
                      <p class="mb-0">Cara Membuat Rekber?</p>
                      <i class="fa" aria-hidden="true"></i>
                    </button>
                  </h2>
                </div>
                <div class="collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="card-body py-3 px-0">
                    <ol>
                      <li>Tambah Teman pada Menu <b>Friends</b></li>
                      <li>Lakukan <b>Request Trade</b></li>
                      <li>Isikan Nominal Rekber</li>
                      <li>Pembeli Lakukan Transfer ke Rekening yang disediakan</li>
                      <li>Setelah Terverifikasi, Lanjutkan Transaksi</li>
                    </ol>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header p-0" id="headingThree" role="tab">
                  <h2 class="mb-0">
                    <button href="#collapseThree" class="d-flex py-3 px-4 align-items-center justify-content-between btn btn-link" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseThree">
                      <p class="mb-0">Kelebihan RekberAja?</p>
                      <i class="fa" aria-hidden="true"></i>
                    </button>
                  </h2>
                </div>
                <div class="collapse" id="collapseThree" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="card-body py-3 px-0">
                    <ol>
                      <li>Website 24 Jam Nonstop</li>
                      <li>Siap Melayani Rekber Kapanpun</li>
                      <li>Robot Yang Memverifikasi Transaksi</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">Transaksi Terakhir</h2>
            <p>Berikut Adalah Transaksi Terakhir Rekber</p>
          </div>
        </div>
        <div class="row ftco-animate justify-content-center">
          <div class="col-md-10">
            <div class="carousel-testimony owl-carousel ftco-owl">
              <div class="item">
                <div class="testimony-wrap d-flex">
                  <div class="user-img mb-4" style="background-image: url('{{ asset('libraries/assets/images/person_1.jpg')}}')">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text">
                    <p class="mb-4">Dadang Telah Menyelesaikan Transaksi dengan <b>Daho Hawley</b></p>
                    <p class="mb-4 text-success">Rp. 10.000.000</p>
                    <p class="name">Dadang Konelo</p>
                    <span class="position">Prajurit</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap d-flex">
                  <div class="user-img mb-4" style="background-image: url(' {{ asset('libraries/assets/images/person_1.jpg')}} ')">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text">
                    <p class="mb-4">Siti Telah Menyelesaikan Transaksi dengan <b>Yusuf Eka</b></p>
                    <p class="mb-4 text-success">Rp. 10.000.000</p>
                    <p class="name">Siti Konelo</p>
                    <span class="position text-info">Perwira</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">RekberAJA</h2>
              <p>Sistem Rekber yang Diverifikasi Dengan Robot Sehingga dapat melayani 24 Jam Nonstop.</p>
              <ul class="ftco-footer-social list-unstyled mb-0">
                <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Promo</a></li>
                <li><a href="#" class="py-2 d-block">Blog</a></li>
                <li><a href="#" class="py-2 d-block">Kontak</a></li>
                <li><a href="#" class="py-2 d-block">Contact</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Navigasi</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Join Us</a></li>
                <li><a href="#" class="py-2 d-block">Blog</a></li>
                <li><a href="#" class="py-2 d-block">Privacy &amp; Policy</a></li>
                <li><a href="#" class="py-2 d-block">Terms &amp; Condition</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Kantor</h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="mr-3 fa fa-map-marker"></span><span class="text">Perum Citra Graha Handayani Cinanjung Kec. Tanjungsari Kabupaten Sumedang Jawa Barat 45362</span></li>
                  <li><a href="#"><span class="mr-3 fa fa-phone"></span><span class="text">+6285794210454</span></a></li>
                  <li><a href="#"><span class="mr-3 fa fa-envelope"></span><span class="text">support@rekberaja.com</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="{{ asset('libraries/assets/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="{{ asset('libraries/assets/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/scrollax.min.js') }}"></script>
<!--   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{ asset('libraries/assets/js/google-map.js') }}"></script> -->
  <script src="{{ asset('libraries/assets/js/main.js') }}"></script>
    
  </body>
</html>