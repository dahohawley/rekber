<!DOCTYPE html>
<html lang="en">
  <head>
    <title>RekberAja By IziTechno</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('libraries/assets/css/animate.css')}} ">
    <link rel="stylesheet" href="{{ asset('libraries/assets/css/flaticon.css')}} ">
    <link rel="stylesheet" href="{{ asset('libraries/assets/css/style.css')}} ">
    {{-- untuk extends css --}}
    @yield('ext_css')
  </head>
  <body>
        <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="{{route('homepage')}}">Rekber Aja</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="{{route('homepage')}}" class="nav-link">Home</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Promo</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Blog</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="hero-wrap js-fullheight">
      <div class="overlay"></div>
          <div class="container-fluid px-0">
            <div class="row d-md-flex no-gutters">
              @yield('content')
            </div>
          </div>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha256-+BEKmIvQ6IsL8sHcvidtDrNOdZO3C9LtFPtF2H0dOHI=" crossorigin="anonymous"></script>
<script src="{{ asset('libraries/assets/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="{{asset('libraries/generic/generic.js')}}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ asset('libraries/assets/js/scrollax.min.js') }}"></script>
<!--   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{ asset('libraries/assets/js/google-map.js') }}"></script> -->
  <script src="{{ asset('libraries/assets/js/main.js') }}"></script>
    {{-- Lokasi kalo nambahin js --}}
    @yield('ext_js')

    {{-- script yang ada dimasing masing view harus didalem section page_script --}}
    @yield('page_script')


  </body>
</html>