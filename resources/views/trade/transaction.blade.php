@extends('layouts.inside')

@section('ext_js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js" integrity="sha512-hktawXAt9BdIaDoaO9DlLp6LYhbHMi5A36LcXQeHgVKUH6kJMOQsAtIw2kmQ9RERDpnSTlafajo6USh9JUXckw==" crossorigin="anonymous"></script>
	<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
@endsection

@section('content')
    <div class="row">

		<!-- Trade List -->
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search">
						<div class="input-group-append">
							<button type="button" class="btn btn-outline-secondary">
								<i class="fa fa-search" aria-hidden="true"></i>	
							</button>
						</div>
					</div>
                </div>
                <div class="card-body" style="height:700px;overflow-y:scroll;" id="trade-list-body">
                    <ul class="list-group" id="trade-list-container">

                    </ul>
                </div>
            </div>
        </div>

		<div id="trade-window" class="col-8" style="display:none;">
			<div class="row">
				<!-- Trade Log -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-2">
									<img src="https://solarkita.com/storage/users/default.png" alt="avatar" style="width:50px;height:50px">
								</div>
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-12">
											<a href="#"  id="trade-window-fullname"></a>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<span class="badge badge-primary" id="trade-window-trade-status">
												wew
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="card-body" id="chat-box" style="height:625px;overflow-y:scroll;">

						</div>
						
						<div class="card-footer">
							<div class="input-group">
								<input type="text" class="form-control" id="chat-message" placeholder="Send message">
								<div class="input-group-append">
									<button type="button" id="send-chat" class="btn btn-outline-primary"> 
										<i class="fa fa-paper-plane" aria-hidden="true"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Trade Item -->
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									Item by lawan trade
								</div>
								<div class="card-body">
									<h4 class="card-title">Title</h4>
									<p class="card-text">Text</p>
								</div>
								<div class="card-footer d-flex justify-content-between">
									<button class="btn btn-outline-secondary">
										<i class="fa fa-unlock" aria-hidden="true"></i> Lock
									</button>

									<button class="btn btn-outline-secondary">
										<i class="fas fa-exchange-alt"></i> Confirm
									</button>

								</div>
							</div>

							<div class="card mt-2">
								<div class="card-header">
									Item by You
								</div>
								<div class="card-body">
									<h4 class="card-title">Title</h4>
									<p class="card-text">Text</p>
								</div>
								<div class="card-footer d-flex justify-content-between">
									<button class="btn btn-outline-secondary">
										<i class="fa fa-unlock" aria-hidden="true"></i> Lock
									</button>

									<button class="btn btn-outline-secondary">
										<i class="fas fa-exchange-alt"></i> Confirm
									</button>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- digunakan kalo transaksinya tidak bisa diapa2in (cancel, rejected, requesting) -->
		<div id="trade-info" class="col-8">
			<div class="card">
				<div class="card-body d-flex justify-content-center align-items-center" style="height:760px;">
					<h5>
						<span id="transaction-status-message">Choose transaction on your left</span>
					</h5>
					<img style="width:157px;height:120px;" src="https://cdn.dribbble.com/users/3369109/screenshots/7186836/media/1ad5bd0c738f8c50bd0b7ca16e3648b8.png"></img>
				</div>
			</div>
		</div>

    </div>

	<li class="list-group-item list-group-item-action trade-item" style="display:none;" id="example-trade-list">
		<div class="row">
			<div class="col-md-2">
				<img src="https://solarkita.com/storage/users/default.png" class="trader-avatar" alt="avatar" style="width:50px;height:50px">
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-10">
						<div class="col-md-12 trader-status">
							<a href="{{route('profile')}}" class="trader-name"></a><br>
						</div>
					</div>
					<div class="col-md-2">
						<div class="dropdown">
							<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-ellipsis-h" aria-hidden="true"></i>
							</button>
							<div class="dropdown-menu dropdown-list" aria-labelledby="dropdownMenuButton">

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</li>

@endsection

@section('page_script')

	<script>
		var userId 	= "{{$user_id}}";
		var vMemoTradeData;
		var vTradeId;
		
		const STATUS_REQUEST 	= 0;
		const STATUS_ACCEPTED 	= 1;
		const STATUS_REJECTED 	= 2;
		const STATUS_CANCELED 	= 3;
		
		let loadTradeList = () => {
			return new Promise(()=>{
				$.ajax({
					url 	: "{{route('tradeList')}}",
					data 	: {
						order : {
							column : "create_dtm",
							dir 	: "desc"
						}
					},
					beforeSend : function(){
						$("#trade-list-body").LoadingOverlay("show");
					},
					success 	: function(res){

						$("#trade-list-container").empty();

						if(res.data){
							vMemoTradeData = res.data;
							rows = res.data;
							$.each(rows,function(col,row){
								traderExample = $("#example-trade-list").clone();
								traderExample.removeAttr('style');
								traderExample.removeAttr('id');

								// isi namanya
								var vTradeStatus = row.trade_status;
								if(row.subject_id == userId){
									var traderData = row.dataRequester;
								}else{
									var traderData = row.dataSubject;
								}

								// Bikin dropdown list & Badfge status
								statusText 	= "";
								dropdownList = "";
								if(row.trade_status == STATUS_REQUEST){
									statusText 	= "<span class=\"badge badge-secondary\">Requesting</span>";
									if(row.dataRequester.user_id != userId){
										/* berarti bisa approve */
										dropdownList += "<a data-trade_id=\""+row.trade_id+"\" data-status_id=\"1\" class=\"dropdown-item btn-action-trade\" href=\"#\"> <span class=\"fa fa-check\"></span> Accept </a>";
										dropdownList += "<a data-trade_id=\""+row.trade_id+"\" data-status_id=\"2\" class=\"dropdown-item btn-action-trade\" href=\"#\"> <span class=\"fa fa-ban\"></span> Reject </a>";
									}else{
										dropdownList += "<a data-trade_id=\""+row.trade_id+"\" data-status_id=\"3\" class=\"dropdown-item btn-action-trade\" href=\"#\"> <span class=\"fa fa-times\"></span> Cancel </a>";
									}

								}else if(row.trade_status == STATUS_ACCEPTED){
									statusText = "<span class=\"badge badge-primary\">Active</span>";
								}else if(row.trade_status == STATUS_CANCELED){
									statusText = "<span class=\"badge badge-warning\">Canceled</span>";
								}else if(row.trade_status == STATUS_REJECTED){
									if(row.dataRequester.user_id != userId){
										statusText = "<span class=\"badge badge-danger\">You reject this</span>";
									}else{
										statusText = "<span class=\"badge badge-danger\">Rejected</span>";
									}
								}

								traderExample.find('.trader-status').append(statusText);
								traderExample.find('.trader-name').attr("href","{{route('profile')}}?id="+traderData.user_id);
								traderExample.find('.trader-name').text(traderData.first_name+ " " +traderData.last_name);

								if(dropdownList == ""){
									traderExample.find(".dropdown-list").parent().remove();
								}else{
									traderExample.find(".dropdown-list").append(dropdownList);
								}
								/* add trade_id data */
								traderExample.attr('data-trade_id',row.trade_id);

								$("#trade-list-container").append(traderExample);
							});
						}
						$("#trade-list-body").LoadingOverlay("hide");
					}
				});
			});
		}

		let fetchBadge = (vRequestStatus) =>{
			if(vRequestStatus == STATUS_REQUEST){
				return "<span class=\"badge badge-secondary\" >Request</span>";
			}else if(vRequestStatus == STATUS_ACCEPTED){
				return "<span class=\"badge badge-primary\" >Active</span>";
			}
		}

		$(()=>{
			loadTradeList();

			$("#chat-message").keypress(function(e){
				if(e.which == 13){
					sendChat();
				}
			});	
			$("#send-chat").click(function(){
				sendChat();
			});
		});

		$(document).on('click','.btn-action-trade',function(){
			data        = $(this).data();
			
			vElementButton = $(this);

			var vTradeID    = data.trade_id;
			var vStatusId   = data.status_id;

			vTradeData   = {};
			/* Cari data memoization nya */
			$.each(vMemoTradeData,function(col,row){
				if(row.trade_id == vTradeID){
					vTradeData = row;
				}     
			});

			$.ajax({
				url     : "{{route('tradeResponse')}}",
				type    : "PUT",
				data    : {
					"_token"    : "{{csrf_token()}}",
					trade_id    : vTradeID,
					status      : vStatusId
				},
				beforeSend      : function(){
					vElementButton.buttonLoader('show');
				},
				complete        : function(){
					loadTradeList();
				}
			});
		});

		// event saat memilih transaksi
		$(document).on('click','.trade-item',function(){
			$.each($(".trade-item"),function(col,row){
				$(row).find(".trader-name").removeAttr('style');

				if($(row).hasClass('active')){
					leavedRow 	= $(row);
					Echo.leaveChannel('trade.' + $(row).data('trade_id') );
					console.log('leaving channel')
				}

				$(row).removeClass('active');
			});
			

			$(this).find(".trader-name").css('color','white');
			$(this).addClass('active');

			vTradeId 			= $(this).data('trade_id');
			var vTradeData;
			// cari trade data dari memoization
			$.each(vMemoTradeData,function(col,row){
				if(row.trade_id == vTradeId){
					vTradeData 	= row;
				}
			});


			/* Listen pusher */
			console.log("Listening : trade."+vTradeId);
			Echo.private('trade.'+vTradeId)
			.listen('TradeActivity',function(e){
				if(e.type_id == 3){
					appendChat(e.payload);
				}
			});

			if(vTradeData.trade_status == STATUS_ACCEPTED){
				$("#trade-info").fadeOut();
				$("#trade-window").fadeIn();

				// data lawan transaksi
				vTradeWith 	= vTradeData.tradeWith;
				$("#trade-window-fullname").attr('href',"{{route('profile')}}?id="+vTradeWith.user_id);
				$("#trade-window-fullname").text(vTradeWith.first_name+" "+vTradeWith.last_name);

				if(vTradeData.trade_status == STATUS_ACCEPTED){
					$("#trade-window-trade-status").text("Active");
					$("#trade-window-trade-status").removeClass();
					$("#trade-window-trade-status").addClass("badge badge-primary");
				}

				
				
				// trade-window-fullname
				// trade-window-trade-status
			}else if(vTradeData.trade_status == STATUS_REJECTED){
				$("#trade-info").fadeIn();
				$("#trade-window").fadeOut();
				$("#transaction-status-message").text("This transaction is in reject state. nothing you can do here....");
			}else if(vTradeData.trade_status == STATUS_REQUEST){
				$("#trade-info").fadeIn();
				$("#trade-window").fadeOut();
				$("#transaction-status-message").text("This transaction is still waiting for approval...");
			}else if(vTradeData.trade_status == STATUS_CANCELED){
				$("#trade-info").fadeIn();
				$("#trade-window").fadeOut();
				$("#transaction-status-message").text("This transaction is canceled");
			}

			loadChat();
		});


		let sendChat = () =>{
			return new Promise((resolve,reject)=>{
				$.ajax({
					url 	: "{{route('tradeSendChat')}}",
					type 	: "POST",
					headers : {
						"X-CSRF-TOKEN" : "{{csrf_token()}}"
					},
					data 	:{
						tId : vTradeId,
						message : $("#chat-message").val()
					},
					beforeSend : function(){
						$("#chat-message").val("");
					},	
					success 	: function(res){
						resolve(res);
					}
				});
			});
		}

		let loadChat = () =>{
			return new Promise(()=>{
				$.ajax({
					url 	: "{{route('tradeLoadChat')}}",
					data 	: {
						tId 	: vTradeId
					},
					success : function(res){
						if(res.status){
							data 	= res.data;
							$.each(data,function(col,row){
								appendChat(row);
							});
						}
					}
				});
			});
		}

		let appendChat = (chatPayload) =>{
			console.log(chatPayload);
			if(chatPayload.created_by == userId){
				
				txt 	=	"<div class=\"row d-flex justify-content-end\">"
				txt 	+=	"	<h3><badge class=\"badge badge-primary\">"+chatPayload.message+"</badge></h3>"
				txt 	+=	"</div>"

			}else{
				txt 	=	"<div class=\"row\">"
				txt 	+=	"	<h3><badge class=\"badge badge-secondary\">"+chatPayload.message+"</badge></h3>"
				txt 	+=	"</div>"
			}
			$("#chat-box").append(txt);
			$('#chat-box').scrollTop($('#chat-box')[0].scrollHeight);
		}

		var pusher = new Pusher('6e5d461794a50ea46e48', {
		cluster: 'ap1'
		});

		var channel = pusher.subscribe('my-channel');
		channel.bind('my-event', function(data) {
		console.log(JSON.stringify(data));
		});
	</script>


@endsection