@extends('layouts.inside')

@section('content')

<!-- Trade List -->
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-status_id="3" id="active-trade-tab" data-toggle="tab" href="#active-trade" role="tab" aria-controls="active-trade" aria-selected="true">
            Active Trade
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="requesting-tab" data-status_id="1" data-toggle="tab" href="#requesting" role="tab" aria-controls="requesting" aria-selected="false">Requesting</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="requested-tab" data-status_id="2" data-toggle="tab" href="#requested" role="tab" aria-controls="requested" aria-selected="false">Waiting for your approval</a>
    </li>
</ul>
<!-- Tab panes -->
<div class="tab-content mt-4">
    <div class="tab-pane active" role="tabpanel" aria-labelledby="active-trade-tab" data-status_id="3">
        <ul id="trade-list">

        </ul>
    </div>
</div>

@endsection

@section('ext_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js" integrity="sha512-hktawXAt9BdIaDoaO9DlLp6LYhbHMi5A36LcXQeHgVKUH6kJMOQsAtIw2kmQ9RERDpnSTlafajo6USh9JUXckw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha512-8vfyGnaOX2EeMypNMptU+MwwK206Jk1I/tMQV4NkhOz+W8glENoMhGyU6n/6VgQUhQcJH8NqQgHhMtZjJJBv3A==" crossorigin="anonymous"></script>
@endsection

@section('page_script')
<script>
    var vStart          = 0;
    var vLength         = 20;
    var vUserId         = "{{$user_id}}";
    var vMemoTradeData  = {};
    var vElementButton;

    const TRADE_ACTIVE      = 3;
    const TRADE_REQUEST     = 1;
    const TRADE_APPROVAL    = 2;

    let loadTrade = (vStatusId = TRADE_ACTIVE) =>{
        return new Promise((resolve,reject)=>{
            $.ajax({
                url     : "{{route('tradeList')}}",
                type    : "GET",
                data    : {
                    status_id   : vStatusId,
                    start       : vStart,
                    length      : vLength
                },
                beforeSend : function(){
                    $("#trade-list").LoadingOverlay("show");
                },
                success : function(res){
                    $("#trade-list").LoadingOverlay("hide");
                    resolve(res);
                },
                error   : function(){
                    $("#trade-list").LoadingOverlay("hide");
                    reject();
                }
            });
        });
    }

    let drawTrade = (list)=>{
        $(".tooltip").remove();
        
        vActiveTab  = $(".nav-link.active").data('status_id');
        $("#trade-list").empty();
        if(list){
            vMemoTradeData = list;
            $.each(list,function(col,row){
                vBtn = "";
                if(vActiveTab == TRADE_ACTIVE){    
                    /* Yah belum belakangan ajah */
                    if(row.subject_id == vUserId){ 
                        rowSubject = row.dataSubject;
                    }else{
                        rowSubject = row.dataRequester;
                    }
                }else if(vActiveTab == TRADE_REQUEST){
                    rowSubject      = row.dataSubject;
                    vBtn            +=  "<button\
                                            class=\"btn btn-danger btn-sm btn-action-trade\" \
                                            data-status_id=\"3\" \
                                            data-trade_id=\""+row.trade_id+"\" \
                                            data-toggle=\"tooltip\" \
                                            data-placement=\"top\" \
                                            title=\"Cancel Request\" \
                                        > \
                                            <i class=\"fa fa-times\"></i> \
                                        </button>";
                    

                }else if(vActiveTab == TRADE_APPROVAL){
                    rowSubject      = row.dataRequester;
                    vBtn            =   "<button\
                                            class=\"btn btn-primary btn-sm btn-action-trade\" \
                                            data-trade_id=\""+row.trade_id+"\" \
                                            data-status_id=\"1\" \
                                            data-action_name=\"accept-trade\" \
                                            data-toggle=\"tooltip\" \
                                            data-placement=\"top\" \
                                            title=\"Accept Trade\" \
                                        > \
                                            <i class=\"fa fa-check\"></i> \
                                        </button>";

                    vBtn            +=  "<button\
                                            class=\"btn btn-danger btn-sm btn-action-trade\" \
                                            data-status_id=\"2\" \
                                            data-trade_id=\""+row.trade_id+"\" \
                                            data-action_name=\"decline-trade\" \
                                            data-toggle=\"tooltip\" \
                                            data-placement=\"top\" \
                                            title=\"Decline\" \
                                        > \
                                            <i class=\"fa fa-times\"></i> \
                                        </button>";
                }

                var vShowName       = rowSubject.first_name + " " + rowSubject.last_name;

                txt     = "";
                txt     += "<li class=\"list-group-item d-flex justify-content-between\">";
                txt     += vShowName;
                txt     += "<div class=\"btn-group\">"+vBtn+"</div>";
                txt     += "</li>";

                $("#trade-list").append(txt);
            });
        }else{
            $("#trade-list").append("<h5> <i class=\"fa fa-ban\"></i> No data exists</h5>");
        }

        $("[data-toggle=\"tooltip\"]").tooltip();
    }

    $(()=>{
        loadTrade(TRADE_ACTIVE).then((response)=>{
            drawTrade(response.data);
        });

        /* Event ganti tab */
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            element = $(e.target);
            data    = element.data();
            loadTrade(data.status_id).then((res)=>{
                drawTrade(res.data);
            }).catch(()=>{
            });
        });
    });

    $(document).on('click','.btn-action-trade',function(){
        data        = $(this).data();
        action      = data.action_name;
        
        vElementButton = $(this);

        var vTradeID    = data.trade_id;
        var vStatusId   = data.status_id;

        vTradeData   = {};
        /* Cari data memoization nya */
        $.each(vMemoTradeData,function(col,row){
            if(row.trade_id == vTradeID){
                vTradeData = row;
            }     
        });

        $.ajax({
            url     : "{{route('tradeResponse')}}",
            type    : "PUT",
            data    : {
                "_token"    : "{{csrf_token()}}",
                trade_id    : vTradeID,
                status      : vStatusId
            },
            beforeSend      : function(){
                vElementButton.buttonLoader('show');
            },
            complete        : function(){
                loadTrade(TRADE_APPROVAL).then((response)=>{
                    drawTrade(response.data);
                });
            }
        });
    });

</script>
@endsection