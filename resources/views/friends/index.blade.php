@extends('layouts.inside')

@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" id="csrf-token">

<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Friend List</h4>
                <hr>
                <nav class="nav nav-pills flex-column flex-sm-row">
                    <a class="flex-sm-fill text-sm-center nav-link active" 
                        id="friend-list-tab" data-toggle="tab" 
                        href="#friend-list" role="tab" 
                        aria-controls="friend-list" aria-selected="true"
                        onclick="loadAcceptedFriend()">
                            Friend List
                    </a>
                    <a class="flex-sm-fill text-sm-center nav-link" 
                        id="friend-request-list-tab" data-toggle="tab" 
                        href="#friend-request-list" role="tab" 
                        aria-controls="friend-request-list" aria-selected="true"
                        onclick="loadFriendRequest();getTotalRequest()">
                            Friend Request <span class="badge badge-info" id="request-counter"><?= $friend_request ?></span>
                    </a>
                </nav>
                <hr>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="friend-list" role="tabpanel" aria-labelledby="friend-list-tab">
                        <div id="container-friend-list">
                            
                        </div>
                    </div>
                    <div class="tab-pane fade" id="friend-request-list" role="tabpanel" aria-labelledby="friend-request-list-tab">
                        <div id="container-friend-request-list">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- Find Friend -->
    <div class="col-md-5">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Find Friend</h4>
                <hr>
                <div class="row" id="row-form-find-user">
                    <div class="col-md-12">
                        <form id="form-find-user">
                            <div class="form-group">
                              <input autocomplete="off" type="text" name="find-user-query" id="find-user-query" class="form-control" placeholder="" aria-describedby="helpId">
                              <small id="helpId" class="text-muted">Type username / email of your friend here</small>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
                <div id="container-found-user">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6" style="display: none;" id="example-row">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-2">
                    <img src="https://t4.ftcdn.net/jpg/00/64/67/63/240_F_64676383_LdbmhiNM6Ypzb3FM4PPuFP9rHe7ri8Ju.jpg" style="width:32px;"></img>
                </div>
                <div class="col-md-7">
                    <h5><a href='#' class="row-full-name" ></a></h5>
                    <p class="row-username"></p>
                </div>
                <div class="col-md-3">
                    <div class="dropdown">
                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Request Trade</a>
                            <a class="dropdown-item btn-remove-friend" href="#">Remove Friend</a>
                            <a class="dropdown-item" href="#">Report</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('ext_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha256-+BEKmIvQ6IsL8sHcvidtDrNOdZO3C9LtFPtF2H0dOHI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js" integrity="sha512-hktawXAt9BdIaDoaO9DlLp6LYhbHMi5A36LcXQeHgVKUH6kJMOQsAtIw2kmQ9RERDpnSTlafajo6USh9JUXckw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha512-8vfyGnaOX2EeMypNMptU+MwwK206Jk1I/tMQV4NkhOz+W8glENoMhGyU6n/6VgQUhQcJH8NqQgHhMtZjJJBv3A==" crossorigin="anonymous"></script>
@endsection

@section('page_script')


<script>
    var offset  = 0;
    var length  = 20;
    var ref_id  = "{{$ref_id}}";

    $(document).ready(function(){
        $("#form-find-user").validate({
            rules   : {
                "find-user-query" : "required"
            },
            submitHandler   : function(){
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url     : "{{route('findFriends')}}",
                    data    : {
                        query : $("#find-user-query").val()
                    },
                    beforeSend : function(){
                        $("#container-found-user").empty();
                        $("#container-found-user").LoadingOverlay("show");
                    },
                    success : function(response){
                        $("#container-found-user").empty();
                        if(response.status){
                            data    = response.data;
                            
                            if(data.countFiltered >= 1){
                                rows        = data.rows;
                                var ref_id  = <?= $ref_id ?>; 
                                $.each(rows,function(key,row){
                                    if(row.is_friend == 0){
                                        buttonAddFriend = "<button title=\"Add Friend\" \
                                            data-subject_id=\""+row.id+"\" \
                                            data-toggle=\"tooltip\" \
                                            data-placement=\"top\" \
                                            class=\"btn btn-primary \
                                            btn-sm btn-add-friend\" >\
                                                <span class=\"fa fa-user-plus\" ></span>\
                                            </button>";
                                    }else if(row.is_friend == 1){
                                        friendData  = row.friend_data;
                                        if(friendData.ref_id == ref_id){
                                            buttonAddFriend = "<button title=\"Cancel Request\" \
                                                data-subject_id=\""+row.id+"\" \
                                                data-friend_id=\""+friendData.id+"\" \
                                                data-toggle=\"tooltip\" \
                                                data-placement=\"top\" \
                                                class=\"btn btn-danger btn-sm btn-cancel-request\" >\
                                                    <span class=\"fa fa-ban\" ></span>\
                                                </button>";
                                        }else{
                                            buttonAddFriend = "<button title=\"Reject Request\" \
                                                data-friend_id=\""+friendData.id+"\" \
                                                data-subject_id=\""+row.id+"\" \
                                                data-toggle=\"tooltip\" \
                                                data-placement=\"top\" \
                                                class=\"btn btn-danger btn-sm btn-reject-request\" >\
                                                    <span class=\"fa fa-ban\" ></span>\
                                                </button> \
                                                \
                                                <button title=\"Accept Request\" \
                                                data-friend_id=\""+friendData.id+"\" \
                                                data-subject_id=\""+row.id+"\" \
                                                data-toggle=\"tooltip\" \
                                                data-placement=\"top\" \
                                                class=\"btn btn-primary btn-sm btn-accept-request\" >\
                                                    <span class=\"fa fa-check\" ></span>\
                                                </button> ";
                                        }
                                    }else if(row.is_friend == 2){
                                        friendData  = row.friend_data;

                                        buttonAddFriend = "<button title=\"Remove Friend\" \
                                            data-friend_id=\""+friendData.id+"\" \
                                            data-toggle=\"tooltip\" \
                                            data-placement=\"top\" \
                                            class=\"btn btn-danger \
                                            btn-sm btn-remove-friend\" >\
                                                <span class=\"fa fa-user-times\" ></span>\
                                            </button>";
                                    }

                                    txt = "<div class=\"row\">"
                                    txt += "    <div class=\"col-md-2\">"
                                    txt += "        <img src=\"https://t4.ftcdn.net/jpg/00/64/67/63/240_F_64676383_LdbmhiNM6Ypzb3FM4PPuFP9rHe7ri8Ju.jpg\" style=\"width:32px;\"></img> "
                                    txt += "    </div> "
                                    txt += "    <div class=\"col-md-7\"> "
                                    txt += "        <h5> "
                                    txt += "            <a href=\"{{url('/user/profile?id=')}}"+row.id+"\">"+row.full_name+"</a> "
                                    txt += "        </h5> "
                                    txt += "        <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Username\"> <i class=\"fa fa-user\"></i> "+row.username+" </span> "
                                    txt += "    </div> "
                                    txt += "    <div class=\"col-md-3\"> "
                                    txt += "        <div class=\"btn-group\"> "
                                    txt += buttonAddFriend;
                                    txt += "        </div> "
                                    txt += "    </div> "
                                    txt += "</div> <hr>";

                                    $("#container-found-user").append(txt);
                                });
                            }else{
                                txt     = "<h5>No user found</h5>";
                                $("#container-found-user").append(txt);
                            }
                        }
                    },
                    complete    : function(){
                        $("[data-toggle=\"tooltip\"]").tooltip();
                        $("#container-found-user").LoadingOverlay("hide");
                    },
                    error   : function(){
                        $("#container-found-user").LoadingOverlay("hide");
                    }
                });
            },
        });
        
        loadAcceptedFriend();

    });

    $(document).on('click','.btn-add-friend',function(){
        data    = $(this).data();
        element = $(this);
        $.ajax({
            url     : "{{route('addFriends')}}",
            type    : "POST",
            headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data    : {
                subject_id  : data.subject_id
            },
            beforeSend : function(){
                element.buttonLoader("show");
            },
            success     : function(response){
                refreshAll();
            },
        });
    });
    $(document).on('click',".btn-cancel-request",function(){
        cancelRequest($(this));
    });
    $(document).on('click',".btn-reject-request",function(){
        cancelRequest($(this));
    });
    $(document).on('click',".btn-remove-friend",function(){
        cancelRequest($(this));
    });
    $(document).on('click','.btn-accept-request',function(){
        data        = $(this).data();
        element     = $(this);
        $.ajax({
            url     : "{{route('acceptFriendRequest')}}",
            type    : "POST",
            headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data    : {
                friend_id   : data.friend_id
            },
            beforeSend : function(){
                element.buttonLoader('show');
            },
            complete    : function(){
                refreshAll();
            },
            error   : function(){
                refreshAll();
            }
        });
    });

    function cancelRequest(element){
        data    = $(element).data();

        $.ajax({
            url     : "{{route('cancelFriendRequest')}}",
            type    : "POST",
            headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data    : {
                friend_id   : data.friend_id
            },
            beforeSend : function(){
                element.buttonLoader("show");
            },
            success     : function(res){
                if(!res.status){
                    // bootbox.alert(res.info);
                }
            },
            complete    : function(){
                element.buttonLoader("hide");
                refreshAll();
            },
            error       : function(){
                element.buttonLoader("hide");
                refreshAll();
            }
            
        })
    }

    function loadAcceptedFriend(){
        /* 1 = Status yang udah di accept */
        return new Promise((resolve,reject)=>{
            $("#container-friend-list").LoadingOverlay('show');
            loadFriendList(2).then((resData)=>{
                data    = resData.rows;
                $("#container-friend-list").empty();
                i = 0;
                $.each(data,function(key,row){
                    rowElement  = $("#example-row").clone();
                    rowElement.removeAttr("style");
                    rowElement.removeAttr("id");
                    rowElement.find(".row-full-name").text(row.subject_data.first_name);
                    rowElement.find(".row-full-name").attr("data-id",row.subject_data.id);
                    rowElement.find(".row-full-name").attr("href","{{url('/user/profile?id=')}}"+row.subject_data.user_id);
                    rowElement.find(".btn-remove-friend").attr("data-friend_id",row.id);

                    rowElement.find(".row-username").text(row.subject_data.username);

                    if(i % 2 == 0){
                        /* Create new row */
                        $("#container-friend-list").append("<div class=\"row\" id=\"container-row-"+i+"\"></div>");
                        $("#container-row-"+i).append(rowElement);
                    }else{
                        $("#container-row-"+ (parseInt(i) - 1)).append(rowElement);
                    }
                    i++;
                });
                $("#container-friend-list").LoadingOverlay('hide');
            });
            resolve();
        });
    }

    function loadFriendRequest(){
        return new Promise((resolve,reject)=>{
            $("#container-friend-request-list").LoadingOverlay('show');
            loadFriendList(1).then((resData)=>{
                data    = resData.rows;
                $("#container-friend-request-list").empty();
                i = 0;
                $.each(data,function(key,row){
                    rowElement  = $("#example-row").clone();
                    rowElement.removeAttr("style");
                    rowElement.removeAttr("id");
                    rowElement.find(".row-full-name").text(row.subject_data.first_name);
                    rowElement.find(".row-full-name").attr("data-id",row.subject_data.id);
                    rowElement.find(".row-full-name").attr("href","{{url('/user/profile?id=')}}"+row.ref_id);
                    rowElement.find(".row-username").text(row.subject_data.username);
                    rowElement.find(".dropdown-menu").empty();

                    /* dropdown menu for request friend */
                    dropDownMenu    = '';
                    dropDownMenu    += '<a class="dropdown-item btn-accept-request" data-friend_id="'+row.id+'"href="#">Accept friend request</a>';
                    dropDownMenu    += '<a class="dropdown-item btn-reject-request" data-friend_id="'+row.id+'" href="#">Reject friend request</a>';
                    rowElement.find('.dropdown-menu').append(dropDownMenu);

                    if(i % 2 == 0){
                        /* Create new row */
                        $("#container-friend-request-list").append("<div class=\"row\" id=\"container-row-request-"+i+"\"></div>");
                        $("#container-row-request-"+i).append(rowElement);
                    }else{
                        $("#container-row-request-"+ (parseInt(i) - 1)).append(rowElement);
                    }
                    i++;
                });
                $("#container-friend-request-list").LoadingOverlay('hide');
            });
            resolve();
        });
    }

    function loadFriendList(status_id = 1){
        var returnData;

        return new Promise((resolve,reject)=>{
            $.ajax({
                url     : "{{route('listFriends')}}",
                data    : {
                    ref_id  : ref_id,
                    length  : length,
                    offset : offset,
                    status_id : status_id,
                    order : {
                        column : "status_id",
                        dir : "asc"
                    }
                },
                success   : function(res){
                    resolve(res);
                },
            });
        });
    }

    function getTotalRequest(){
        return new Promise((resolve)=>{
            $.ajax({
                url     : "{{route('countTotalFriendRequest')}}",
                success : function(response){
                    resolve(response)
                }
            });
        }).then((response)=>{
            countTotal  = response.data;
            $("#request-counter").text(countTotal);
        });
    }

    function refreshAll(){
        loadAcceptedFriend();
        loadFriendRequest();
        getTotalRequest();
        $("#form-find-user").submit();
    }

</script>

@endsection

@section('ext_css')

<style>
    .media.media-xs .media-object {
        width: 32px;
    }
</style>

@endsection