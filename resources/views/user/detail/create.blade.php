@extends('layouts.inside')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <form id="form-detail-user" >
        <div class="row">
            <div class="col-md-8">
                <div class="row mb-4">
                    <div class="col">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" placeholder="First name" id="first_name" name="first_name" >
                    </div>
                    <div class="col">
                        <label for="first_name">Last Name</label>
                        <input type="text" class="form-control" placeholder="Last name" id="last_name" name="last_name" >
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col">
                        <label for="first_name">Birth Date</label>
                        <input type="text" class="form-control" style="background-color: white;" placeholder="Birth Date" id="birth_date" name="birth_date" readonly>
                    </div>
                    <div class="col">
                        <label for="first_name">Birth Place</label>
                        <input type="text" class="form-control" placeholder="Birth Place" id="birth_place" name="birth_place"  >
                    </div>
                </div>

                <div class="form-group">
                  <label for="">About Me</label>
                  <textarea name="about" class="form-control" id="about"></textarea>
                </div>

            </div>

            <div class="col-md-4 d-flex flex-row-reverse bd-highlight">
                <div id="image-box" style="width: 200px; height:200px; background-color:black;"></div>
            </div>

            <button class="btn btn-primary float-right" id="btn-save" >Save</button>

        </div>
    </form>
@endsection

@section('page_script')
    <script>
        $(document).ready(function(){
            $("#birth_date").datepicker();

            $("#form-detail-user").validate({
                rules   : {
                    "first_name"    : "required",
                    "last_name"     :"required",
                    "birth_date"    : "required",
                    "birth_place"    : "required",
                },
                submitHandler : function(){
                    $.ajax({
                        url     : "{{route('saveProfile')}}",
                        type    : "POST",
                        headers  : {
                            "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
                        },
                        data    :{
                            first_name : $("#first_name").val(),
                            last_name : $("#last_name").val(),
                            birth_date : function(){
                                return moment($("#birth_date").val()).format('YYYY-MM-DD');
                            },
                            birth_place : $("#birth_place").val(),
                            about : $("#about").val(),
                        },
                        beforeSend : function(){
                            $("#btn-save").buttonLoader('show','');
                        },
                        success     : function(res){
                            if(res.success){
                                window.location = "{{url(route('home'))}}"
                            }
                        }
                    });
                }
            })

        });
    </script>
@endsection


@section('ext_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js" integrity="sha256-ZsWP0vT+akWmvEMkNYgZrPHKU9Ke8nYBPC3dqONp1mY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha256-+BEKmIvQ6IsL8sHcvidtDrNOdZO3C9LtFPtF2H0dOHI=" crossorigin="anonymous"></script>
@endsection

@section('ext_css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css" integrity="sha256-FAOaXTpl90/K8cXmSdsskbQN3nKYulhCpPbcFzGTWKI=" crossorigin="anonymous" />
@endsection

