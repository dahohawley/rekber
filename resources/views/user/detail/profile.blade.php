@extends('layouts.inside')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <img class="card-img-top" style="max-height:300px; max-width:355px;!important" src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F-zsbDeAUd8aY%2FUS7F0ta5d9I%2FAAAAAAAAEKY%2FUL2AAhHj6J8%2Fs1600%2Ffacebook-default-no-profile-pic.jpg&f=1&nofb=1" alt="">
                        <div class="card-body">
                            <h4 class="card-title">{{$user['first_name'].' '.$user['last_name']}}</h4>
                            <p class="card-text"> <i class="fa fa-envelope" aria-hidden="true"></i> {{$user['email']}}</p>
                            <p class="card-text"> <i class="fa fa-birthday-cake" aria-hidden="true"></i> {{date('d F Y',strtotime($detail['birth_date']))}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group">
                        @if (!$is_friend)
                            <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Request Friends"> <i class="fa fa-user-plus" aria-hidden="true"></i> </button>
                        @endif

                        @if(!$my_profile)
                            <a href="{{route('tradeRequest')}}?id={{$user['user_id']}}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Request Trade"> <i class="fa fa-exchange-alt" aria-hidden="true"></i> </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron jumbotron-fluid">
                        <div class="container">
                            <h1 class="display-4">About</h1>
                            <p class="lead">{{$detail['about']}}</p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="timeline-tab" data-toggle="tab" href="#timeline" role="tab" aria-controls="timeline" aria-selected="true">Timeline</a>
                        </li>
                    </ul>
                    <div class="tab-content mt-2" id="myTabContent">
                        <div class="tab-pane fade show active" id="timeline" role="tabpanel" aria-labelledby="timeline-tab">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card" style="display:none;" id="card-activity">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8 activity-description">
                </div>
                <div class="col-md-4 d-flex justify-content-end activity-time">
                </div>
            </div>
        </div>
    </div>
    <hr>
@endsection

@section('ext_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-loading-overlay/2.1.7/loadingoverlay.min.js" integrity="sha512-hktawXAt9BdIaDoaO9DlLp6LYhbHMi5A36LcXQeHgVKUH6kJMOQsAtIw2kmQ9RERDpnSTlafajo6USh9JUXckw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js" integrity="sha512-rmZcZsyhe0/MAjquhTgiUcb4d9knaFc7b5xAfju483gbEXTkeJRUMIPk6s3ySZMYUHEcjKbjLjyddGWMrNEvZg==" crossorigin="anonymous"></script>
@endsection

@section('page_script')
    <script>
        var start = 0;
        var length = 10;

        $(()=>{
            $("[data-toggle=\"tooltip\"]").tooltip();
            loadActivity().then((response)=>{
                $.each(response,function(col,row){
                    element     = $("#card-activity").clone().removeAttr("id").removeAttr('style');
                    element.find(".activity-description").html(row.description);

                    createDtmTxt    = "<i class=\"fa fa-clock\" aria-hidden=\"true\">"+moment(row.create_dtm).format("DD MMMM YYYY H:i:s")+"</i>";
                    element.find(".activity-time").html(moment(row.create_dtm).format("DD MMMM YYYY H:m:s"));
                    $("#timeline").append(element);
                });
            });
        });

        let loadActivity = () => {
            return new Promise((resolve,reject)=>{
                $.ajax({
                    url     : "{{route('getActivity')}}",
                    type    : "GET",
                    data    : {
                        user_id : "{{$user['user_id']}}",
                        start : start,
                        length : length,
                    },
                    success : function(response){
                        $("#timeline").LoadingOverlay('hide');
                        resolve(response);
                    },
                    beforeSend : function(){
                        $("#timeline").LoadingOverlay('show');
                    },
                    complete : function(){
                        $("#timeline").LoadingOverlay('hide');
                    }
                });
            });
        }

    </script>
@endsection


