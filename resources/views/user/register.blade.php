@extends('layouts.form')

@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" id="csrf-token">

<div class="col-md-7">
    <center><img class="one-third js-fullheight" src="{{ asset('libraries/assets/images/undraw_sign_in_e6hj.svg')}}" alt="" width="70%"></center>
</div>
<div class="col-md-5">
    <div class="one-forth d-flex align-items-center ftco-animate js-fullheight">
        <div class="card" style="width: 70%">
            <div class="card-body">
                <h4 class="card-title"><center>DAFTAR AKUN BARU</center></h4>
                <div id="alert-container">

                </div>
                <hr>
                <form id="form-register" autocomplete="off" id="card-registration">
                    <div class="form-group">
                      <label for="">Username <sup class="text-danger">*</sup> </label>
                      <input type="text" name="username" id="username" class="form-control" placeholder="" aria-describedby="helpId" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail <sup class="text-danger">*</sup></label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="" aria-describedby="helpId" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="">Password <sup class="text-danger">*</sup></label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="" aria-describedby="helpId" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="">Conf Password <sup class="text-danger">*</sup> </label>
                        <input type="password" name="conf_password" id="conf_password" class="form-control" placeholder="" aria-describedby="helpId" autocomplete="off">
                    </div>

                    <div class="row d-flex justify-content-center">
                        <div class="col-md-12">
                            <a class="btn btn-secondary btn-sm pull-left" href="{{url('/login')}}"><i class="fa fa-sign-in"></i> Back To Login</a>
                            <button class="btn btn-primary btn-sm my-auto pull-right" id="btn-register">
                                <i class="fa fa-key"></i> Register
                            </button>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_script')

<script>
    $(document).ready(function(){
        $("#form-register").validate({
            rules   : {
                username : "required",
                email   : "required",
                password : "required",
                conf_password : {
                    "required" : true,
                    "equalTo"  :"#password"
                }
            },
            submitHandler   : function(){
                $.ajax({
                    url     : "{{url('/register')}}",
                    type    : "POST",
                    data    : {
                        username    : $("#username").val(),
                        email       : $("#email").val(),
                        password    : $("#password").val(),
                        conf_password : $("#conf_password").val()
                    },
                    beforeSend : function(){
                        $(".alert").hide();
                        $("#btn-register").buttonLoader('show',"Register");
                    },
                    success     : function(res){
                        response    = res;
                        if(response.success){
                            $("#alert-container").append(
                                "<div class='alert alert-success'>Please check your email to complete registration</div>"
                            );
                            $("#card-registration").hide();
                        }else{
                            $("#alert-container").append(
                                "<div class='alert alert-danger'>"+res.info+"</div>"
                            );
                        }
                        return;
                    },
                    complete    : function(){
                        $("#btn-register").buttonLoader('hide',"Register");
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }
        });
    });
</script>

@endsection