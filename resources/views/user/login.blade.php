@extends('layouts.form')

@section('content')
<div class="col-md-7">
    <center><img class="one-third js-fullheight" src="{{ asset('libraries/assets/images/undraw_secure_login_pdn4.svg')}}" alt="" width="70%"></center>
</div>
<div class="col-md-5">
    <div class="one-forth d-flex align-items-center ftco-animate js-fullheight">
      <div class="card" style="width: 70%">
          <div class="card-body">
            @if (session('message'))
                <div class="alert alert-danger">
                    {{ session('message') }}
                </div>
            @endif
              <form class="form-horizontal" id="form-login" action="{{route('loginAction')}}" method="POST">
                  @csrf
                  <div class="form-group">
                      <label for="username">Username / E-Mail</label>
                      <input type="text" class="form-control" name="username" id="username">
                  </div>
                  <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" name="password" id="password">
                  </div>
                  <button class="btn btn-primary btn-block" id="btnLogin" type="submit">Login</button>
              </form>
              <hr>
              <div class="row d-flex justify-content-end">
                <div class="col-md-6">
                    <label><input type="checkbox" name="" value=""> Remember Me</label>
                </div>
                  <div class="col-md-6">
                      <a class="btn btn-secondary btn-sm float-right" href="{{url('/register')}}"><i class="fa fa-key"></i> Register</a>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>
@endsection
